﻿namespace System.Collections.Generic
{
    public static class HashSetExtensions
    {
        public static void AddMissing<T>(this HashSet<T> o, IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                if (!o.Contains(item))
                    o.Add(item);
            }
        }

        public static void AddMissing<T>(this HashSet<T> o, T item)
        {
            if (!o.Contains(item))
                o.Add(item);            
        }
    }
}

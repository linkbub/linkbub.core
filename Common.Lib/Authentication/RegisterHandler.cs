﻿using Common.Lib.Infrastructure;

namespace Common.Lib.Authentication
{
    public class RegisterHandler : IDependencyInjectable
    {
        public IDependencyContainer DepCon { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public LoginHandler LoginHandler { get; set; }

        public RegisterHandler()
        {

        }

        public RegisterHandler(string email, string password)
        {
            var newUser = new User
            {
                Email = email,
                Password = password
            };

            //var sr = newUser.Save();

            //if (sr.IsSuccess)
            //{
            //    IsSuccess = true;
            //    LoginHandler = new LoginHandler();
            //    LoginHandler.RequestLogin(sr.Value);
            //}
            //else
            //{
            //    IsSuccess = false;
            //    Message = sr.SaveUserResult.ToString();
            //}
        }
    }
}


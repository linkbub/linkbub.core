﻿using Common.Lib.Core.Context;
using Common.Lib.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Linq;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Authentication
{
    public class LoginHandler : IDependencyInjectable
    {
        public IDependencyContainer DepCon { get; set; }

        static ConcurrentDictionary<Guid, LoginHandler> CurrentTokens
        {
            get
            {
                return _currentTokens ?? (_currentTokens = new ConcurrentDictionary<Guid, LoginHandler>());
            }
        }
        static ConcurrentDictionary<Guid, LoginHandler> _currentTokens;

        [JsonIgnore]
        public User User { get; set; }

        public Guid UserId
        {
            get
            {
                return User == null ? default(Guid) : User.Id;
            }
        }

        public string UserEmail
        {
            get
            {
                return User == null ? string.Empty : User.Email;
            }
        }

        public string Token { get; set; }
        public DateTime CreatedOn { get; set; }

        public int HoursToExpire { get; set; } = 1;

        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public LoginHandler()
        {

        }

        public LoginHandler RequestLogin(User input)
        {
            using (var repo = DepCon.ResolveRepository<User>())
            {
                var existingUser = repo.FirstOrDefault(x => x.Email == input.Email && x.Password == input.Password);

                if (existingUser != null)
                {
                    IsSuccess = true;

                    if (CurrentTokens.ContainsKey(existingUser.Id))
                    {
                        var existingToken = CurrentTokens[existingUser.Id];

                        if ((DateTime.Now - existingToken.CreatedOn).TotalMinutes > existingToken.HoursToExpire * 60)
                        {
                            var newToken = new LoginHandler()
                            {
                                Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("=", ""),
                                IsSuccess = true,
                                CreatedOn = DateTime.Now,
                                HoursToExpire = 2,
                                User = existingUser
                            };

                            CurrentTokens[existingUser.Id] = newToken;

                            return newToken;
                        }
                        else
                        {
                            existingToken.CreatedOn = DateTime.Now;
                            return existingToken;
                        }
                    }
                    else
                    {
                        var newToken = new LoginHandler()
                        {
                            Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("=", ""),
                            IsSuccess = true,
                            CreatedOn = DateTime.Now,
                            HoursToExpire = 2,
                            User = existingUser
                        };
                        CurrentTokens.TryAdd(existingUser.Id, newToken);
                        return newToken;
                    }
                }
                else
                {
                    var newToken = new LoginHandler()
                    {
                        IsSuccess = false,
                        Message = "Wrong email or password"
                    };
                    return newToken;
                }
            }
        }
    }
}


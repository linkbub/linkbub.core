﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Common.Lib.Authentication
{
     public class User_Name_eq_value : BaseExpression
    {
        public string Name { get; set; }

        public override Func<TEntity, bool> Build<TEntity>()
        {
            return (x) => (x as User).Name == Name;
        }

        public override Expression<Func<TEntity, bool>> CreateExpression<TEntity>()
        {
            return (x) => (x as User).Name == Name;
        }

        public override object[] GetParameters()
        {
            return new object[] { Name };
        }

        public override void SetParameters(object[] parameters)
        {
            Name = parameters[0].ToString();
        }

        public static User_Name_eq_value Create(string value)
        {
            return new User_Name_eq_value() { Name = value };
        }
    }
}

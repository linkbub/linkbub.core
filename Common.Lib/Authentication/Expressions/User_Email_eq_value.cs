﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Common.Lib.Authentication
{
    public class User_Email_eq_value : BaseExpression
    {
        public string Email { get; set; }

        public override Func<TEntity, bool> Build<TEntity>()
        {
            return (x) => (x as User).Email == Email;
        }

        public override Expression<Func<TEntity, bool>> CreateExpression<TEntity>()
        {
            return (x) => (x as User).Email == Email;
        }

        public override object[] GetParameters()
        {
            return new object[] { Email };
        }

        public override void SetParameters(object[] parameters)
        {
            Email = parameters[0].ToString();
        }

        public static User_Email_eq_value Create(string value)
        {
            return new User_Email_eq_value() { Email = value };
        }
    }
}

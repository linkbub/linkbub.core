﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Common.Lib.Authentication
{
    public class User_Email_And_Password_eq_values : BaseExpression
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public override Func<TEntity, bool> Build<TEntity>()
        {
            return (x) => (x as User).Email == Email && (x as User).Password == Password;
        }

        public override Expression<Func<TEntity, bool>> CreateExpression<TEntity>()
        {
            return (x) => (x as User).Email == Email && (x as User).Password == Password;
        }

        public override object[] GetParameters()
        {
            return new object[] { Email, Password };
        }

        public override void SetParameters(object[] parameters)
        {
            Email = parameters[0].ToString();
            Password = parameters[1].ToString();
        }

        public static User_Email_And_Password_eq_values Create(string email, string password)
        {
            return new User_Email_And_Password_eq_values() { Email = email, Password = password };
        }
    }
}

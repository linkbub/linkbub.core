﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Metadata;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif


namespace Common.Lib.Authentication
{
    public class User : Entity
    {
#region static

        public static IClientRepository<User> ClientRepository
        {
            get
            {
                return (IClientRepository<User>)Repository;
            }
        }
        public static IRepository<User> Repository
        {
            get
            {
                return Repository<User>();
            }
        }


        public static string HashPassword(string password)
        {
            var output = Cryptography.Tools.GetSHA256Hash(password);
            return output;
        }

        public static async Task<QueryResult<User>> FindAsync(Guid id)
        {
            using (var repo = Repository)
            {
                return await repo.FindAsync(id);
            }
        }



#endregion

        [Metadata(UserMetadata.Name)]
        public string Name { get; set; }
        
        [Metadata(UserMetadata.Surname1)]
        public string Surname1 { get; set; }

        [Metadata(UserMetadata.Surname2)]
        public string Surname2 { get; set; }

        [Metadata(UserMetadata.Email)]
        public string Email { get; set; }

        [JsonIgnore]
        [Metadata(UserMetadata.Password)]
        public string Password { get; set; }

        public User()
        {
            SaveAction = async (actionInfo, trace) => (await SaveAsync(actionInfo, trace)).CastTo<Entity>();
            DeleteAction = async (actionInfo, trace) => (await DeleteAsync(actionInfo, trace));
            CloneAction = Clone;
        }

#region Clone
        public virtual User Clone()
        {
            return Clone<User>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as User;

            output.Name = Name;
            output.Surname1 = Surname1;
            output.Surname2 = Surname2;
            output.Email = Email;
            output.Password = Password;

            return output as T;
        }

#endregion

#region Changes

        public override EntityChanges GetChanges()
        {
            var output = base.GetChanges();

            if (ParentEntity == null)
            {
                if (!string.IsNullOrEmpty(Name))
                    output.AddChange(UserMetadata.Name, Name);

                if (!string.IsNullOrEmpty(Surname1))
                    output.AddChange(UserMetadata.Surname1, Surname1);

                if (!string.IsNullOrEmpty(Surname2))
                    output.AddChange(UserMetadata.Surname2, Surname2);

                if (!string.IsNullOrEmpty(Email))
                    output.AddChange(UserMetadata.Email, Email);

                if (!string.IsNullOrEmpty(Password))
                    output.AddChange(UserMetadata.Password, Password);

            }
            else
            {
                var previous = ParentEntity as User;

                if (previous.Name != Name)
                    output.AddChange(UserMetadata.Name, Name);

                if (previous.Surname1 != Surname1)
                    output.AddChange(UserMetadata.Surname1, Surname1);

                if (previous.Surname2 != Surname2)
                    output.AddChange(UserMetadata.Surname2, Surname2);

                if (previous.Email != Email)
                    output.AddChange(UserMetadata.Email, Email);

                if (previous.Password != Password)
                    output.AddChange(UserMetadata.Password, Password);
            }


            return output;
        }

        public override PropertiesHandler CreatePropertiesHandler()
        {
            var propHandler = base.CreatePropertiesHandler();

            propHandler.RegisterSetter(UserMetadata.Name, (o) => { Name = ConvertToString(o); });
            propHandler.RegisterSetter(UserMetadata.Surname1, (o) => { Surname1 = ConvertToString(o); });
            propHandler.RegisterSetter(UserMetadata.Surname2, (o) => { Surname2 = ConvertToString(o); });
            propHandler.RegisterSetter(UserMetadata.Email, (o) => { Email = ConvertToString(o); });
            propHandler.RegisterSetter(UserMetadata.Password, (o) => { Password = ConvertToString(o); });

            return propHandler;
        }

#endregion

#region Save

        public Task<QueryResult<User>> SaveAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return this.SaveAsync<User>(null, actionInfo, trace);
        }
        public override async Task<QueryResult<T>> SaveAsync<T>(QueryResult<T> qr = null, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var slr = await SaveLogic<T>();
            
            if (slr.IsSuccess)
            {
                return await base.SaveAsync<T>(qr, actionInfo, trace); 
            }
            else
            {
                return new QueryResult<T>() { IsSuccess = false, ResultNum = (int)SaveEntityResultsTypes.LogicError };
            }
        }

        public async Task<QueryResult> SaveLogic<T>() where T : Entity, new()
        {
            return await Task.Run(() =>
            {
                if (Context.ContextMode == ContextModesTypes.ClientMode)
                {
                    return new QueryResult() { IsSuccess = true };
                }
                else
                {
                    using (var repoUsers = Context.DepCon.ResolveRepository<T>())
                    {
                        // do logic
                        if (this.Id == default(Guid) && repoUsers.Any(x => (x as User).Email == this.Email))
                        {
                            return new QueryResult() { IsSuccess = false, ResultNum = (int)SaveUserResultsTypes.TheEmailIsAlreadyRegistered };
                        }
                        else
                        {
                            return new QueryResult() { IsSuccess = true };
                        }
                    }
                }
            });
        }

#endregion

#region Delete

        public void DeleteUow(IUnitOfWork uow)
        {
            this.Delete(uow);
        }

        public async Task<QueryResult> DeleteAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await this.DeleteAsync<User>(null, actionInfo, trace);
        }

        public override async Task<QueryResult> DeleteAsync<T>(QueryResult qr = null, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var dlr = await DeleteLogic();

            if (dlr.IsSuccess)
            {
                return await base.DeleteAsync<T>(qr, actionInfo, trace);
            }
            else
            {
                return new QueryResult() { IsSuccess = false, ResultNum = (int)SaveUserResultsTypes.LogicError };
            }
        }

        public async Task<QueryResult> DeleteLogic()
        {
            return await Task.Run(() =>
            {
                if (Context.ContextMode == ContextModesTypes.ClientMode)
                {
                    return new QueryResult() { IsSuccess = true };
                }
                else
                {
                    return new QueryResult() { IsSuccess = true };
                }
            });
        }

#endregion

#region Serialization

        public override void Serialize(IQueryResultSerializer serializer, bool includeChildren)
        {
            base.Serialize(serializer, includeChildren);

            serializer.AddEntityProperty(this.TypeName, this.Id, UserMetadata.Name, Name);
            serializer.AddEntityProperty(this.TypeName, this.Id, UserMetadata.Surname1, Surname1);
            serializer.AddEntityProperty(this.TypeName, this.Id, UserMetadata.Surname2, Surname2);
            serializer.AddEntityProperty(this.TypeName, this.Id, UserMetadata.Email, Email);
            //serializer.AddEntityProperty(this.TypeName, this.Id, UserMetadata.Password, Password);
        }

        public override void Deserialize(IQueryResultSerializer deserializer, Dictionary<int, string> data)
        {
            base.Deserialize(deserializer, data);

            if (data.ContainsKey(UserMetadata.Name))
                Name = data[UserMetadata.Name];

            if (data.ContainsKey(UserMetadata.Surname1))
                Surname1 = data[UserMetadata.Surname1];

            if (data.ContainsKey(UserMetadata.Surname2))
                Surname2 = data[UserMetadata.Surname2];

            if (data.ContainsKey(UserMetadata.Email))
                Email = data[UserMetadata.Email];

            if (data.ContainsKey(UserMetadata.Password))
                Password = data[UserMetadata.Password];
        }

#endregion
    }


    public class UserQueryResult : QueryResult<User>
    {
        public SaveUserResultsTypes SaveUserResult
        {
            get
            {
                return (SaveUserResultsTypes)ResultNum;
            }
            set
            {
                ResultNum = (int)value;
            }
        }
    }

    public enum SaveUserResultsTypes
    {
        Ok = SaveEntityResultsTypes.Ok,
        DbAddError = SaveEntityResultsTypes.DbAddError,
        DbUpdateError = SaveEntityResultsTypes.DbUpdateError,
        DbDeleteError = SaveEntityResultsTypes.DbDeleteError,
        LogicError = SaveEntityResultsTypes.LogicError,
        TheEmailIsAlreadyRegistered
    }

    public static class UserMetadata
    {
        public const int Name = EntityMetadata.Last + 1;
        public const int Surname1 = Name + 1;
        public const int Surname2 = Surname1 + 1;
        public const int Email = Surname2 + 1;
        public const int Password = Email + 1;

        public const int Last = Password;
    }
}

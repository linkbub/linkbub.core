﻿using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Lib.Serialization
{
    public class ChangesSerializer : IChangesSerializer
    {
        public static string FieldSeparator = "|";
        public static string ValueSeparator = "¬";

        public static string PartSeparator = "{";
        public static string ChangeSeparator = "[";

        List<ChangeInfo> ChangesInfos { get; set; }

        public IDependencyContainer DepCon { get; set; }

        public ChangesSerializer()
        {
            ChangesInfos = new List<ChangeInfo>();
        }

        public void AddEntityChanges(EntityChanges entityChanges, string entityType)
        {
            ChangeTypes changeType = ChangeTypes.Add;

            if (entityChanges.IsRemoved)
                changeType = ChangeTypes.Delete;

            else if (entityChanges.IsNewMainEntity)
                changeType = ChangeTypes.Add;

            else
                changeType = ChangeTypes.Update;


            ChangesInfos.Add(new ChangeInfo()
            {
                Time = entityChanges.CreatedOn,
                MainEntityId = entityChanges.MainEntityId,
                TypeName = entityType,
                ChangeType = changeType,
                Changes = entityChanges.Changes,
                RangeCode = string.Empty
            });
        }

        public void AddEntityChanges(List<EntityChanges> entityChanges, string entityType)
        {
            foreach (var change in entityChanges)
                AddEntityChanges(change, entityType);
        }

        public void AddChangeInfo(ChangeInfo changeInfo)
        {
            ChangesInfos.Add(changeInfo);
        }

        public string GetSerializedChanges()
        {
            var encoded = string.Empty;
            var prechanges = new List<string>();

            foreach (var changeInfo in ChangesInfos.OrderBy(x => x.Time))
            {
                var part1 = new List<string>();
                if (changeInfo.MainEntityId != default(Guid))
                    part1.Add("E" + ValueSeparator + changeInfo.MainEntityId.ToString());

                part1.Add("T" + ValueSeparator + changeInfo.TypeName);
                part1.Add("C" + ValueSeparator + (int)changeInfo.ChangeType);

                if (!string.IsNullOrEmpty(changeInfo.RangeCode))
                    part1.Add("R" + ValueSeparator + changeInfo.RangeCode);

                var schanges = string.Join(FieldSeparator, part1);

                var part2 = new List<string>();

                foreach (var change in changeInfo.Changes)
                {
                    part2.Add(QueryResultSerializer.GetPairValue(change.MetadataId.ToString(), change.Value));
                }

                schanges += PartSeparator + string.Join(FieldSeparator, part2);
                prechanges.Add(schanges);
            }

            var serialized = string.Join(ChangeSeparator, prechanges);
            encoded = QueryResultSerializer.EncodeTo64(serialized);

            return encoded;
        }

        public List<ChangeInfo> DeserializeChanges(string encoded)
        {
            var output = new List<ChangeInfo>();
            var decoded = QueryResultSerializer.DecodeFrom64(encoded);

            var sChanges = decoded.Split(ChangeSeparator[0]);

            for (var i = 0; i < sChanges.Length; i++)
            {
                var changeInfo = new ChangeInfo();

                var parts = sChanges[i].Split(PartSeparator[0]);

                if (parts.Length < 2)
                {
                    var a = sChanges[i];
                    var b = a;
                }

                var subInfo = parts[0].Split(FieldSeparator[0]);

                foreach (var s in subInfo)
                {
                    var pairValue = s.Split(ValueSeparator[0]);

                    switch (pairValue[0])
                    {
                        case "E":
                            changeInfo.MainEntityId = new Guid(pairValue[1]);
                            break;

                        case "T":
                            changeInfo.TypeName = pairValue[1];
                            break;

                        case "C":
                            changeInfo.ChangeType = (ChangeTypes)int.Parse(pairValue[1]);
                            break;

                        case "R":
                            changeInfo.RangeCode = pairValue[1];
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(parts[1]))
                {                    
                    var changes = parts[1].Split(FieldSeparator[0]);

                    for (var j = 0; j < changes.Length; j++)
                    {
                        var pairKeyValue = changes[j].Split(ValueSeparator[0]);
                        changeInfo.Changes.Add(new ChangeUnit() { MetadataId = int.Parse(pairKeyValue[0]), Value = QueryResultSerializer.ReplaceWildcards(pairKeyValue[1]) });
                    }
                }

                output.Add(changeInfo);
            }

            return output;

        }

        public void Dispose()
        {

        }
    }
}

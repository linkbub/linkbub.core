﻿using Common.Lib.Core;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Lib.Serialization
{
    public class QueryResultSerializer : IQueryResultSerializer
    {
        public static UTF8Encoding Encoder = new UTF8Encoding();

        public static string DefaultQueryResultSuccess
        {
            get
            {
                return _defaultQueryResultSuccess ?? (_defaultQueryResultSuccess = CommonSerializer.SerializeAction(new QueryResult() { IsSuccess = true, ResultNum = 1 }));
            }
        }
        static string _defaultQueryResultSuccess;
        
        public static string MainAndSecondaryNodesSeparator = "·";
        public static string NodeTypeSeparator = "]";
        public static string EntityTypesSeparator = "~";
        public static string NodeCodeSeparator = "]";
        public static string FieldSeparator = "|";
        public static string ValueSeparator = "¬";

        public static string QueryResultSeparator = "{";
        public static string NodeSeparator = "}";
        public static string IdArraySeparator = ",";

        static bool _isComaFormatInit = false;
        static bool _isComaUsedInDoubles = false;
        public static bool IsComaUsedInDoubles
        {
            get
            {
                if (!_isComaFormatInit)
                {
                    _isComaFormatInit = true;

                    var numString = 8.8.ToString();
                    _isComaUsedInDoubles = numString.Contains(",");
                }

                return _isComaUsedInDoubles;
            }
        }

        public static Dictionary<string, string> Wildcards = new Dictionary<string, string>()
        {
            {"·", "$$P_U:N_T:O$$"},
            {"|", "$$B_A:R_R:A$$"},
            {"¬", "$$G_A:N_CHI:TO$$"},
            {"~", "$$A_P:OS_TRO:FE$$"},
            {",", "$$C_O:M_A$$"},
            {"[", "$$C_O:R_CH1$$"},
            {"]", "$$C_O:R_CH2$$"},
            {"{", "$$LL_A:V_E1$$"},
            {"}", "$$LL_A:V_E2$$"}
        };

        public static Dictionary<string, string> WildcardsJs = new Dictionary<string, string>()
        {
            {"·", "$$$P_U:N_T:O$$$"},
            {"|", "$$$B_A:R_R:A$$$"},
            {"¬", "$$$G_A:N_CHI:TO$$$"},
            {"~", "$$$A_P:OS_TRO:FE$$$"},
            {",", "$$$C_O:M_A$$$"},
            {"[", "$$$C_O:R_CH1$$$"},
            {"]", "$$$C_O:R_CH2$$$"},
            {"{", "$$$LL_A:V_E1$$$"},
            {"}", "$$$LL_A:V_E2$$$"}
        };

        private QueryResult QueryResult { get; set; }
        

        public IDependencyContainer DepCon { get; set; }

        int AllEntitiesCounter = 0;
        Dictionary<Guid, int> AllEntitiesCodesById { get; set; } = new Dictionary<Guid, int>();
        Dictionary<int, Entity> AllEntitiesByCode { get; set; } = new Dictionary<int, Entity>();

        List<ChildrenData> ChildrenDataList { get; set; } = new List<ChildrenData>();

        /// <summary>
        ///  1st string: entity type to group
        ///  Guid: entity id
        ///  2nd string: serialized entity (wihtout nested information which will be in the SecondaryNodes)
        /// </summary>
        Dictionary<string, Dictionary<Guid, List<string>>> MainNodes { get; set; }

        /// <summary>
        /// int: the subnode number
        /// string: the subnode serialized (complex types in tables)
        /// </summary>
        Dictionary<int, List<string>> SecondaryNodes { get; set; }

        Dictionary<string, string> SecondaryNodesStrings { get; set; }

        Dictionary<string, ReferencedImageData> ReferencedImagesData { get; set; }

        List<Action> ChildrenAssignActions { get; set; } = new List<Action>();

        int SecondayNodeCode = 0;

        public QueryResultSerializer()
        {
            MainNodes = new Dictionary<string, Dictionary<Guid, List<string>>>();
            ReferencedImagesData = new Dictionary<string, ReferencedImageData>();
            SecondaryNodes = new Dictionary<int, List<string>>();
        }

        public void AddEntity(Entity entity, bool includeChildren)
        {
            AllEntitiesCodesById.Add(entity.Id, AllEntitiesCounter++);

            if (!MainNodes.ContainsKey(entity.TypeName))
                MainNodes.Add(entity.TypeName, new Dictionary<Guid, List<string>>());

            if (!MainNodes[entity.TypeName].ContainsKey(entity.Id))
            {
                MainNodes[entity.TypeName].Add(entity.Id, new List<string>());
                entity.Serialize(this, includeChildren);
            }
        }

        public void AddReferencedImageData(ReferencedImageData refImg)
        {
            if (!ReferencedImagesData.ContainsKey(refImg.ImageId))
                ReferencedImagesData.Add(refImg.ImageId, refImg);
            else
                ReferencedImagesData[refImg.ImageId] = refImg;
        }

        public void AddEntityProperty(string parentType, Guid parentId, int metadataId, object value)
        {
            var pairValue = GetPairValue(metadataId.ToString(), value);
            MainNodes[parentType][parentId].Add(pairValue);
        }                

        public void AddChildrenEntityProperty(string parentType, Guid parentId, int metadataId, string propertyTypeName, List<Guid> ids)
        {
            var s = new ChildrenData();
            s.ParentType = parentType;
            s.ParentId = parentId;
            s.MetadataId = metadataId;
            s.PropertyTypeName = propertyTypeName;
            s.Ids = ids;

            ChildrenDataList.Add(s);
        }

        public void AddChildrenEntityProperty(string parentType, Guid parentId, int metadataId, string propertyTypeName, Guid id)
        {
            var s = new ChildrenData();
            s.ParentType = parentType;
            s.ParentId = parentId;
            s.MetadataId = metadataId;
            s.PropertyTypeName = propertyTypeName;
            s.Ids = new List<Guid>() { id };

            ChildrenDataList.Add(s);
        }

        public void AddChildrenEntityProperty(string parentType, Guid parentId, int metadataId, string propertyTypeName, Guid? id)
        {
            if (id.HasValue)
                AddChildrenEntityProperty(parentType, parentId, metadataId, propertyTypeName, id.Value);
            else
                AddChildrenEntityProperty(parentType, parentId, metadataId, propertyTypeName, default(Guid));
        }

        #region  Entity Changes

        public void AddEntityChanges(EntityChanges entityChanges, Guid mainEntityId)
        {

        }

        #endregion


        public static string GetPairValue(string getterKey, object value)
        {
            var valueAsString = AdaptValue(value);

            var pairKeyValue = getterKey + ValueSeparator + valueAsString;
            return pairKeyValue;
        }

        public static string AdaptValue(object value)
        {
            //Todo: replace serialization Symbols

            if (value == null)
                return string.Empty;

            if (value is System.Enum)
                return ((int)value).ToString();

            if (value is double)
            {
                var output = IsComaUsedInDoubles ? value.ToString().Replace(",", ".") : value.ToString();
                return output;
            }

            if (value is string)
                return AddWildcards(value.ToString());

            if (value is int || value is bool || value is double || value is Guid)
                return value.ToString();

            if (value is DateTime)
                return ((DateTime)value).Ticks.ToString();

            if (value is ReferencedImage)
                return ((ReferencedImage)value).Id;

            if (value is List<Guid>)
                return string.Join(IdArraySeparator, (value as List<Guid>));


            if (value is List<int>)
                return string.Join(IdArraySeparator, (value as List<int>));

            if (value is byte[])
                return EncodeTo64(value as byte[]);

            throw new Exception("value is not in any valid format type:" + value.GetType().FullName + " value:" + value.ToString());
        }


        public static string AddWildcards(string value)
        {
#if CORE_JS
            foreach (var item in WildcardsJs)
#else
            foreach (var item in Wildcards)
#endif
            {
                value = value.Replace(item.Key, item.Value);
            }
            return value;
        }

        public static string ReplaceWildcards(string value)
        {
            //#if CORE_JS
            //            foreach (var item in WildcardsJs)
            //#else
            //            foreach (var item in Wildcards)
            //#endif
            //            {
            //                value = value.Replace(item.Value, item.Key);
            //            }

            foreach (var item in Wildcards)
            {
                if (value.Contains(item.Value))
                    value = value.Replace(item.Value, item.Key);
                else if (value.Contains(WildcardsJs[item.Key]))
                    value = value.Replace(WildcardsJs[item.Key], item.Key);
            }

            return value;
        }

        #region Encoding

        public static string EncodeTo64(byte[] toEncode)
        {
            var returnValue = Convert.ToBase64String(toEncode);
            return returnValue;
        }

        public static string EncodeTo64(string toEncode)
        {
            var toEncodeAsBytes = Encoder.GetBytes(toEncode);
            var returnValue = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string DecodeFrom64(string encodedData)
        {
            // por si ya viene decodificado, esto pasa por el cambio de core 2 a 3
            if (encodedData[0] == '{' || encodedData[1] == '¬' )
                return encodedData;

            byte[] encodedDataAsBytes = null;

            encodedDataAsBytes = Convert.FromBase64String(encodedData);

            var returnValue = Encoder.GetString(encodedDataAsBytes, 0, encodedDataAsBytes.Length);
            return returnValue;
        }

        public static byte[] DecodeFrom64ToByteArray(string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            return encodedDataAsBytes;
        }

        #endregion

        public override string ToString()
        {
            var Sb = new StringBuilder();

            #region QueryResul Info

            if (QueryResult != null)
            {
                var trace = QueryResult.Trace == null ? string.Empty : "|T¬" + QueryResult.Trace;
                Sb.Append("I¬" + QueryResult.IsSuccess + "|M¬" + QueryResult.Message + "|P¬" + QueryResult.AvailablePages + trace + QueryResultSeparator);
            }

            #endregion

            #region SecondayNodes

            var nodesStrings = new string[SecondaryNodes.Count];
            var nodeCount = SecondaryNodes.Count;

            foreach (var nodeItem in SecondaryNodes)
            {
                nodeCount--;
                nodesStrings[nodeCount] = nodeItem.Key + NodeCodeSeparator + string.Join(FieldSeparator, nodeItem.Value);
            }

            Sb.Append(string.Join(NodeSeparator, nodesStrings));

            #endregion

            Sb.Append(MainAndSecondaryNodesSeparator);

            #region MainNodes
            var nodesTypesStrings = new string[MainNodes.Count];
            var nodeTypeCount = 0;
            foreach (var nodeTypeItem in MainNodes)
            {
                nodesStrings = new string[nodeTypeItem.Value.Count];
                nodeCount = 0;

                foreach (var nodeItem in nodeTypeItem.Value)
                {
                    var counter = AllEntitiesCodesById[nodeItem.Key];

                    nodesStrings[nodeCount] = "0¬" + counter + FieldSeparator + string.Join(FieldSeparator, nodeItem.Value);
                    nodeCount++;
                }

                
                nodesTypesStrings[nodeTypeCount] = nodeTypeItem.Key + EntityTypesSeparator + String.Join(NodeSeparator, nodesStrings);
                nodeTypeCount++;
            }
            Sb.Append(string.Join(NodeTypeSeparator, nodesTypesStrings));

            #endregion

            Sb.Append(MainAndSecondaryNodesSeparator);

            #region ReferencedImages

            if (ReferencedImagesData.Count > 0)
            {
                Sb.Append(string.Join(FieldSeparator, ReferencedImagesData.Values.Select(x => x.ToString())));
            }

            #endregion

            Sb.Append("}");

            return Sb.ToString();
        }

        public string ToStringEncoded()
        {
            var output = ToString();
            return output;
            //return EncodeTo64(output);
        }

        public void Dispose()
        {
            MainNodes = null;
            SecondaryNodes = null;
        }

        public void AddQueryResult(QueryResult queryResult)
        {
            QueryResult = queryResult;
           
            if (queryResult.ObjectValue != null)
            {
                if (queryResult.ObjectValue is Entity)
                {
                    var entity = queryResult.ObjectValue as Entity;
                    AddEntity(entity, queryResult.TypeIncludeChildren(entity.TypeName));
                }
                else if (queryResult.ObjectValue is ReferencedImageData)
                {
                    AddReferencedImageData(queryResult.ObjectValue as ReferencedImageData);
                }
                else if (queryResult.ObjectValue is IList)
                {
                    var enu = (IList)queryResult.ObjectValue;
                    if (enu.Count > 0)
                    {
                        var includeChildren = false;
                        var e = enu[0] as Entity;

                        if (e != null)
                            includeChildren = queryResult.TypeIncludeChildren(e.TypeName);

                        foreach (var value in enu)
                        {
                            if (value is Entity)
                            {
                                AddEntity(value as Entity, includeChildren);
                            }
                        }
                    }
                }
            }

            if (QueryResult.MultiResultEntities != null && QueryResult.MultiResultEntities.Count > 0)
            {
                foreach (var item in QueryResult.MultiResultEntities)
                {
                    var includeChildren = queryResult.TypeIncludeChildren(item.Key);

                    foreach (Entity entity in item.Value)
                    {
                        AddEntity(entity, includeChildren);
                    }
                }
            }

            //Replace Guid by int codes
            foreach (var cd in ChildrenDataList)
            {
                var codes = new List<int>();

                foreach (var id in cd.Ids)
                {
                    codes.Add(AllEntitiesCodesById[id]);
                }

                var pairValue = GetPairValue(cd.MetadataId.ToString(), codes);
                MainNodes[cd.ParentType][cd.ParentId].Add(pairValue);
            }
        }

        public QueryResult<T> DeserializeQueryResult<T>(string input, OperationInfo trace = null) where T : class, new()
        {
            try
            {
                //if (trace != null)
                //    trace.AddStep($"Start preparing deserializing for expected type {typeof(T).Name}");
                //var requestedOn = DateTime.Now;

                if (input == "ScKsVHJ1ZXxNwqx7wrfCt30=")
                {
                    var tt = typeof(T);
                    return new QueryResult<T>()
                    {
                        IsSuccess = true,
                        Message = string.Empty,
                        Value = MetadataHandler.ModelsByListType.ContainsKey(tt) ?
                                    (T)MetadataHandler.ModelsByListType[tt].ListConstructor() :
                                    default(T)
                    };
                }

                var output = new QueryResult<T>();

                var tType = typeof(T);

                var decoded = DecodeFrom64(input);
                var spaso1 = decoded.Split(QueryResultSeparator[0]);
                var totalResults = 0;

                #region Get QueryResult info

                var qrData = spaso1[0].Split(FieldSeparator[0]);

                //var qrData1 = qrData[0].Split(ValueSeparator[0]);
                //var qrData2 = qrData[1].Split('¬');

                output.IsSuccess = qrData[0] == "I¬True";
                //output.Message = qrData2[1];
                output.Message = qrData[1].Split('¬')[1];
                //output.AvailablePages = int.Parse(qrData2[2]);
                //output.AvailablePages = int.Parse(qrData[2].Split('¬')[1]);

                if (qrData.Length == 4)
                {
                    var qrData3 = qrData[3].Split('¬');
                    output.Trace = OperationInfo.FromString(qrData3[1]);
                }

                #endregion

                #region Get ReferencedImageData
                var spaso2 = spaso1[1].Split(MainAndSecondaryNodesSeparator[0]);

                if (typeof(T) == typeof(ReferencedImageData))
                {
                    if (spaso2.Length > 0)
                    {
                        var imgChains = spaso2[2].Split(FieldSeparator[0]);
                        output.Value = ReferencedImageData.FromString(imgChains[0].Replace("}", string.Empty)) as T;
                    }
                    else
                    {
                        output.Value = default(T);
                    }
                    return output;
                }
                else if (typeof(T) == typeof(List<ReferencedImageData>))
                {
                    if (spaso2.Length > 0)
                    {
                        var imgChains = spaso2[2].Split(FieldSeparator[0]);
                        var list = new List<ReferencedImageData>();
                        foreach (var imgChain in imgChains)
                        {
                            list.Add(ReferencedImageData.FromString(imgChains[0].Replace("}", string.Empty)));
                        }
                        output.Value = list as T;
                    }
                    else
                    {
                        output.Value = default(T);
                    }
                    return output;
                }

                #endregion

                #region Get Subnodes Info

                var subNodesPart = spaso2[0].Split(NodeSeparator[0]);
                SecondaryNodesStrings = new Dictionary<string, string>();

                for (var i = 0; i < subNodesPart.Length; i++)
                {
                    if (string.IsNullOrEmpty(subNodesPart[i]))
                        break;

                    var snodePaso = subNodesPart[i].Split(NodeCodeSeparator[0]);
                    SecondaryNodesStrings.Add(snodePaso[0], snodePaso[1]);
                }

                if (decoded.EndsWith("{··}"))
                {
                    output.Value = default(T);
                    return output;
                }

                #endregion

                #region Get nodes
                var entityTypesPart = spaso2[1].Split(NodeTypeSeparator[0]);

                var entities = new Dictionary<string, IList>();

                //if (trace != null)
                //{
                //    var totalTime = (DateTime.Now - requestedOn).TotalMilliseconds;
                //    trace.AddStep($"Finished preparing deserializing for expected type {typeof(T).Name} total time:{totalTime}ms");
                //    requestedOn = DateTime.Now;
                //}

                for (var i = 0; i < entityTypesPart.Length; i++)
                {
                    var entityParts = entityTypesPart[i].Split(EntityTypesSeparator[0]);

                    var entitiesType = entityParts[0];
                    var entitiesStrings = entityParts[1].Split(NodeSeparator[0]);

                    var list = MetadataHandler.ModelsByTypeName[entitiesType].ListConstructor();
                    var entityCtor = MetadataHandler.ModelsByTypeName[entitiesType].Constructor;

                    entities.Add(entitiesType, list);

                    if (trace != null)
                        trace.AddStep($"deserialize entities of type {entitiesType}");

                    var requestedOn = DateTime.Now;

                    for (var j = 0; j < entitiesStrings.Length; j++)
                    {
                        if (!string.IsNullOrEmpty(entitiesStrings[j]))
                        {
                            int entityCode = 0;
                            Dictionary<int, string> data = GetData(entitiesStrings[j], ref entityCode);

                            var newEntity = entityCtor();
                            newEntity.Deserialize(this, data);

                            AllEntitiesByCode.Add(entityCode, newEntity);
                            AllEntitiesCodesById.Add(newEntity.Id, entityCode);

                            list.Add(newEntity);
                            totalResults++;
                        }                        
                    }

                    // Todo: refactor adding name-Type dictionary
                    if (list is T)
                    {
                        if (output.Value == null)
                            output.Value = list as T;
                        else
                            (output.Value as IList).Add(list as T);

                        if (output.MultiResultEntities != null)
                            output.MultiResultEntities.Add(entitiesType, list);
                    }
                    else if (tType.FullName == entitiesType || MetadataHandler.ModelsByTypeName[entitiesType].IsChildOf(tType))
                    {
                        if (list.Count == 0)
                            output.Value = default(T);
                        else if (list.Count == 1)
                            output.Value = list[0] as T;
                        else
                        {
                            var o = list[0];
                            output.Value = o as T;
                            list.Remove(o);
                        }
                    }
                    else if (MetadataHandler.ModelsByTypeName[entitiesType].IsCollectionTypeOf(tType))
                    {
                        if (output.Value == null)
                            output.Value = new T();

                        var asList = output.Value as IList;

                        foreach (object item in list)
                        {
                            asList.Add(item);
                        }

                        if (output.MultiResultEntities != null)
                            output.MultiResultEntities.Add(entitiesType, list);
                    }
                    else
                    {
                        if (output.MultiResultEntities == null)
                            output.MultiResultEntities = new Dictionary<string, IList>();

                        output.MultiResultEntities.Add(entitiesType, list);
                    }

                    if (trace != null)
                    {
                        var totalTime = (DateTime.Now - requestedOn).TotalMilliseconds;
                        var avg = Math.Round(totalResults / totalTime, 3);

                        trace.AddStep($"deserialized {entitiesType} total time:{totalTime}ms results: {totalResults} avg: {avg}e/ms");
                    }

                }

                #endregion

                #region Assign children

                foreach (var action in ChildrenAssignActions)
                    action();

                #endregion

                output.Trace = trace;

                return output;
            }
            catch (Exception e1)
            {
                var a = e1;
                var o = a;

                throw e1;
            }
        }

        public QueryResult DeserializeQueryResult(string input)
        {
            //if (input == "ScKsVHJ1ZXxNwqx7wrd9")
            if (input == DefaultQueryResultSuccess)
                return new QueryResult() { IsSuccess = true, Message = string.Empty, ResultNum = 1 };

            var output = new QueryResult();

            var decoded = DecodeFrom64(input);
            var spaso1 = decoded.Split(QueryResultSeparator[0]);

            #region Get QueryResult info

            var qrData = spaso1[0].Split(FieldSeparator[0]);

            var qrData2 = qrData[1].Split('¬');

            output.IsSuccess = qrData[0] == "I¬True";
            output.Message = qrData2[1];
            output.AvailablePages = 0;

            if (qrData.Length == 4)
            {
                var qrData3 = qrData[3].Split('¬');
                output.Trace = OperationInfo.FromString(qrData3[1]);
            }

            if (decoded.EndsWith("{··}"))
                return output;

            #endregion

            #region Get Subnodes Info

            var spaso2 = spaso1[1].Split(MainAndSecondaryNodesSeparator[0]);
            var subNodesPart = spaso2[0].Split(NodeSeparator[0]);
            SecondaryNodesStrings = new Dictionary<string, string>();

            for (var i = 0; i < subNodesPart.Length; i++)
            {
                if (string.IsNullOrEmpty(subNodesPart[i]))
                    break;

                var snodePaso = subNodesPart[i].Split(NodeCodeSeparator[0]);
                SecondaryNodesStrings.Add(snodePaso[0], snodePaso[1]);
            }

            #endregion

            #region Get nodes
            var entityTypesPart = spaso2[1].Split(NodeTypeSeparator[0]);

            var entities = new Dictionary<string, IList>();

            for (var i = 0; i < entityTypesPart.Length; i++)
            {
                var entityParts = entityTypesPart[i].Split(EntityTypesSeparator[0]);

                var entitiesType = entityParts[0];
                var entitiesStrings = entityParts[1].Split(NodeSeparator[0]);

                var list = MetadataHandler.ModelsByTypeName[entitiesType].ListConstructor();
                var entityCtor = MetadataHandler.ModelsByTypeName[entitiesType].Constructor;

                entities.Add(entitiesType, list);

                for (var j = 0; j < entitiesStrings.Length; j++)
                {
                    if (!string.IsNullOrEmpty(entitiesStrings[j]))
                    {
                        int entityCode = 0;
                        Dictionary<int, string> data = GetData(entitiesStrings[j], ref entityCode);

                        var newEntity = entityCtor();
                        newEntity.Deserialize(this, data);
                        list.Add(newEntity);
                    }
                }

                output.MultiResultEntities.Add(entitiesType, list);
            }

            #endregion

            return output;

        }

        public Dictionary<int, string> GetData(string entityString, ref int entityCode)
        {
            var data = new Dictionary<int, string>();
            var sEntityPaso = entityString.Split(FieldSeparator[0]);

            for (var k = 0; k < sEntityPaso.Length; k++)
            {
                var sFieldValue = sEntityPaso[k].Split(ValueSeparator[0]);

                if (sFieldValue[0] == "0")
                    entityCode = int.Parse(sFieldValue[1]);
                else
                    data.Add(int.Parse(sFieldValue[0]), ReplaceWildcards(sFieldValue[1]));
            }

            return data;
        }


        public List<Guid> GetIds(string value)
        {
            var sIds = value.Split(IdArraySeparator[0]);
            //return sIds.Select(id => new Guid(id)).ToList();

            var output = new List<Guid>();

            foreach (var id in sIds)
            {
                if (!string.IsNullOrEmpty(id))
                    output.Add(new Guid(id));
            }

            return output;
        }

        public string[] GetRows(string subnodes)
        {
            var spasoIds = subnodes.Split(IdArraySeparator[0]);
            var output = new string[spasoIds.Length];

            for (var i = spasoIds.Length - 1; i > -1; i--)
            {
                output[i] = SecondaryNodesStrings[spasoIds[i]];
            }

            return output;
        }

        public Dictionary<int, string> GetRowData(string rowId)
        {
            int entityCode = 0;
            return GetData(GetRow(rowId), ref entityCode);
        }

        public string GetRow(string subnode)
        {
            return SecondaryNodesStrings[subnode];
        }

        public string DoubleToString(double value)
        {
            throw new NotImplementedException();
        }

        public double ParseDouble(string value)
        {
            if (IsComaUsedInDoubles && value.Contains("."))
            {
                var inputValue = value;
                value = value.Replace('.', ',');
            }
            else if (!IsComaUsedInDoubles && value.Contains(","))
            {
                var inputValue = value;
                value = value.Replace(',', '.');
            }
            else
            {
            }
            return double.Parse(value);
        }

        public void RegisterAddChildren<T>(string data, Action<List<T>> action) where T : Entity
        {
            var assignAction = new Action(() =>
            {
                var codes = GetCodes(data);

                var entities = new List<T>();
                foreach (var code in codes)
                    entities.Add(AllEntitiesByCode[code] as T);

                action(entities);
            });

            ChildrenAssignActions.Add(assignAction);
        }

        public void RegisterAddOneChildren<T>(int code, Action<T> action) where T : Entity
        {
            var assignAction = new Action(() =>
            {
                action(AllEntitiesByCode[code] as T);
            });

            ChildrenAssignActions.Add(assignAction);
        }

        public void RegisterTryAddOneChildren<T>(Guid id, Action<T> action) where T : Entity
        {
            var assignAction = new Action(() =>
            {
                if (AllEntitiesCodesById.ContainsKey(id))
                {
                    var code = AllEntitiesCodesById[id];
                    action(AllEntitiesByCode[code] as T);
                }
            });

            ChildrenAssignActions.Add(assignAction);
        }

        public void RegisterTryAddOneChildren<T>(Guid? id, Action<T> action) where T : Entity
        {
            if (id.HasValue)
                RegisterTryAddOneChildren(id.Value, action);
        }

        public List<int> GetCodes(string data)
        {
            var output = new List<int>();

            if (data.Length > 0)
            {
                var spaso = data.Split(',');
                foreach (var s in spaso)
                    output.Add(int.Parse(s));
            }

            return output;
        }

        internal struct ChildrenData
        {
            internal string ParentType;
            internal Guid ParentId;
            internal int MetadataId;
            internal string PropertyTypeName;
            internal List<Guid> Ids;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Common.Lib.Core;
using Common.Lib.Services;

namespace Common.Lib.Serialization
{
    public static class CommonSerializer
    {
        public static UTF8Encoding Encoder = new UTF8Encoding();

        /// <summary>
        /// ObjectOpeningSeparator "{"
        /// </summary>
        public static string OOSep = "{";

        /// <summary>
        /// ObjectEndingSeparator = "}";
        /// </summary>
        public static string OESep = "}";

        //internal static object Serialize(List<QueryableChainItem> items)
        //{
        //    throw new NotImplementedException();
        //}

        public static string SerializeAndEncode(object input)
        {
            return EncodeTo64(SerializeAction(input));
        }

        public static T DeserializeEncoded<T>(string input)
        {
            return DeserializeAction<T>(DecodeFrom64(input));
        }

        /// <summary>
        /// Field Separator "|"
        /// </summary>
        public static string FSep = "|";

        /// <summary>
        /// Value Separator
        /// </summary>
        public static string VSep = "¬";

        /// <summary>
        /// ArraySeparator ","
        /// </summary>
        public static string ASep = ",";

        public static Func<object, string> SerializeAction { get; set; }

        public static Func<string, Type, object> DeserializeAsObjectAction { get; set; }

        public static string SerializeArray(HashSet<int> items)
        {
            return OOSep + string.Join(ASep, items) + OESep;
        }

        //public static string SerializeArray(List<QueryableChainItem> items)
        //{
        //    return OOSep + string.Join(ASep, items) + OESep;
        //}

        public static List<T> DeserializeArray<T>(string input, Func<string, T> conversor) where T : new()
        {
            var output = new List<T>();

            if (input.StartsWith(OOSep) && input.EndsWith(OESep))
                input = input.Substring(1, input.Length - 2);

            var spaso = input.Split(ASep[0]);

            spaso.ToList().ForEach((s) =>
            {
                output.Add(conversor(s));
            });

            return output;
        }

        public static void ProcesPairItems(string input, Action<KeyValuePair<string, object>> processAction)
        {
            if (input.StartsWith(OOSep) && input.EndsWith(OESep))
                input = input.Substring(1, input.Length - 2);

            var spaso = input.Split(FSep[0]);

            foreach (var pair in spaso)
            {
                var spaso2 = pair.Split(VSep[0]);
                processAction(new KeyValuePair<string, object>(spaso2[0].Replace("Â", string.Empty), spaso2[1]));
                //Todo: we do the replace because Bridge.net transpiles the keys adding a Â, so try to fix it in Bridge
            }
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = Encoder.GetBytes(toEncode);
            var returnValue = Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        public static string EncodeTo64(byte[] toEncode)
        {
            var returnValue = Convert.ToBase64String(toEncode);
            return returnValue;
        }

        public static string DecodeFrom64(string encodedData)
        {
            if (encodedData.Contains("["))
                return encodedData;

            // Todo: reivew cases when the data comes not encoded when it supposed to
            if (encodedData != null && encodedData.Length > 0 && encodedData[0] == OOSep[0])
                return encodedData;

            byte[] encodedDataAsBytes = null;

            encodedDataAsBytes = Convert.FromBase64String(encodedData);

            var returnValue = Encoder.GetString(encodedDataAsBytes, 0, encodedDataAsBytes.Length);
            return returnValue;
        }

        public static byte[] DecodeFrom64ToByteArray(string encodedData)
        {
            // Todo: reivew cases when the data comes not encoded when it supposed to
            if (encodedData != null && encodedData.Length > 0 && encodedData[0] == OOSep[0])
                return new byte[0];

            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            return encodedDataAsBytes;
        }

        public static T DeserializeAction<T>(string s)
        {
            return (T)DeserializeAsObjectAction(s, typeof(T));
        }

        public static string DoubleToString(double value)
        {
            return value.ToString().Replace(",", ".");
        }

        public static double ParseDouble(string value)
        {
            if (QueryResultSerializer.IsComaUsedInDoubles && value.Contains("."))
            {
                var inputValue = value;
                value = value.Replace('.', ',');
            }
            else if (!QueryResultSerializer.IsComaUsedInDoubles && value.Contains(","))
            {
                var inputValue = value;
                value = value.Replace(',', '.');
            }
            else
            {
            }
            return double.Parse(value);
        }

        /// <summary>
        /// Compresses the string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static string Zip(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            var memoryStream = new MemoryStream();
            using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
            {
                gZipStream.Write(buffer, 0, buffer.Length);
            }

            memoryStream.Position = 0;

            var compressedData = new byte[memoryStream.Length];
            memoryStream.Read(compressedData, 0, compressedData.Length);

            var gZipBuffer = new byte[compressedData.Length + 4];
            Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
            Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
            return Convert.ToBase64String(gZipBuffer);
        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string Unzip(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }
    }


}

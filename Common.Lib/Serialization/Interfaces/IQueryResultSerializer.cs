﻿using System;
using Common.Lib.Core;
using Common.Lib.Services;
using System.Collections.Generic;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Infrastructure.ActionsResults;

namespace Common.Lib.Serialization
{
    public interface IQueryResultSerializer : IDependencyInjectable, IDisposable
    {
        void AddQueryResult(QueryResult queryResult);

        void AddEntity(Entity entity, bool includeChildren);

        #region  Add to entity

        void AddEntityProperty(string parentType, Guid parentId, int metadataId, object value);

        void AddChildrenEntityProperty(string parentType, Guid parentId, int metadataId, string propertyTypeName, Guid id);
        void AddChildrenEntityProperty(string parentType, Guid parentId, int metadataId, string propertyTypeName, Guid? id);

        void AddChildrenEntityProperty(string parentType, Guid parentId, int metadataId, string propertyTypeName, List<Guid> ids);

        #endregion

        Dictionary<int, string> GetData(string entityString, ref int entityCode);

        List<Guid> GetIds(string value);

        QueryResult<T> DeserializeQueryResult<T>(string input, OperationInfo trace = null) where T : class, new();

        QueryResult DeserializeQueryResult(string input);

        void RegisterAddChildren<T>(string data, Action<List<T>> action) where T : Entity;
        void RegisterAddOneChildren<T>(int code, Action<T> action) where T : Entity;
        void RegisterTryAddOneChildren<T>(Guid id, Action<T> action) where T : Entity;
        void RegisterTryAddOneChildren<T>(Guid? id, Action<T> action) where T : Entity;
        string DoubleToString(double value);

        double ParseDouble(string value);

        string ToStringEncoded();
    }
}

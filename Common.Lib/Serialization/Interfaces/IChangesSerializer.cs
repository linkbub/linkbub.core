﻿using System;
using System.Collections.Generic;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;

namespace Common.Lib.Serialization
{
    public interface IChangesSerializer : IDependencyInjectable, IDisposable
    {
        void AddEntityChanges(EntityChanges entityChanges, string entityType);

        void AddEntityChanges(List<EntityChanges> entityChanges, string entityType);

        void AddChangeInfo(ChangeInfo changeInfo);

        string GetSerializedChanges();

        List<ChangeInfo> DeserializeChanges(string encoded);
    }
}

﻿using Common.Lib.Core;
using Common.Lib.Core.Tracking;
using Common.Lib.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services.Operations
{
    public class AddOrUpdateSingleEntity
    {
        //[JsonProperty(PropertyName = "ei")]
        public Guid EntityId { get; set; }
        
        //[JsonProperty(PropertyName = "se")]
        public string SerializedChanges { get; set; }

        public void SetChanges(Entity entity)
        {
            EntityId = entity.Id;
            using (var serializer = Entity.Context.DepCon.ResolveAndConstruct<IChangesSerializer>())
            {
                entity.GetChanges().Serialize(serializer);
                SerializedChanges = serializer.GetSerializedChanges();
            }
        }

        public List<ChangeInfo> GetChanges()
        {
            using (var serializer = Entity.Context.DepCon.ResolveAndConstruct<IChangesSerializer>())
            {
                return serializer.DeserializeChanges(SerializedChanges);
            }
        }
    }
}

﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Tracking;
using Common.Lib.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services.Operations
{
    public class UnitOfWorkInfo
    {       
        //[JsonProperty(PropertyName = "se")]
        public List<UoWActInfo> UoWActInfos { get; set; }               

        public void SetChanges(IUnitOfWork uow)
        {
            UoWActInfos = uow.UowActions;
        }

    }
}

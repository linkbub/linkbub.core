﻿using System;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services.Operations
{
    public class DeleteSingleEntity
    {
        public Guid EntityId { get; set; }

        //[JsonProperty(PropertyName = "tn")]
        public string TypeName { get; set; }
    }
}

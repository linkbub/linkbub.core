﻿namespace Common.Lib.Services.Operations
{
    public enum RepositoryQueryTypes
    {
        Where = 0,
        FirstOrDefault = 1,
        Any = 2,
        Find = 3,
        Count = 4,
        GetAll = 5
    }
}

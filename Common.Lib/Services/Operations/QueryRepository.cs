﻿using Common.Lib.Core;
using Common.Lib.Core.Metadata;
using Common.Lib.Serialization;
using System;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services.Operations
{
    public class QueryRepository
    {
        //[JsonProperty(PropertyName = "id")]
        public Guid QueryId { get; set; }

        //[JsonProperty(PropertyName = "se")]
        public string SerializedExpression { get; set; }

        //[JsonProperty(PropertyName = "et")]
        public string ExpressionType { get; set; }

        //[JsonProperty(PropertyName = "qt")]
        public RepositoryQueryTypes QueryType { get; set; }

        //[JsonProperty(PropertyName = "rt")]
        public string RepoType { get; set; }

        //[JsonProperty(PropertyName = "ps")]
        public int PageSize { get; set; }

        //[JsonProperty(PropertyName = "pn")]
        public int PageNumber { get; set; }

        public bool PopulateBabylonConcepts { get; set; }
        public ChildrenPolicyTypes ChildrenPolicyType { get; set; }

        [JsonIgnore]
        public IExpressionBuilder ExpressionBuilder
        {
            get
            {
                if (_expressionBuilder == null && !(string.IsNullOrEmpty(SerializedExpression)))
                {
                    var t = Entity.Context.DepCon.ResolveType(ExpressionType);
                    var o = CommonSerializer.DeserializeAsObjectAction(SerializedExpression, t);
                    _expressionBuilder = (IExpressionBuilder)o;
                }

                return _expressionBuilder;
            }
            set
            {
                _expressionBuilder = value;
                SerializedExpression = CommonSerializer.SerializeAction(value);
                ExpressionType = value.GetType().FullName;
            }
        }
        IExpressionBuilder _expressionBuilder;

    }
}

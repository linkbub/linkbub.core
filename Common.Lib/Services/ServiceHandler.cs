﻿using Common.Lib.Core;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Serialization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Lib.Services
{
    public static class ServiceHandler
    {
        public static Dictionary<string, Type> ServicesRequestTypes { get; set; }

        public static async Task<TResponse> Request<TResponse>(ServiceRequest request, OperationInfo trace = null) where TResponse : ServiceResponse
        {
            var svcInvoker = new ServiceInvoker();
            var paramsCarrier = new ParamsCarrier
            {
                ServiceRequestInputParams = CommonSerializer.SerializeAction(request),
                ServiceRequestType = request.GetType().FullName,
                OperationType = OperationTypes.ServiceRequest,
                Trace = trace
            };

            var serviceResult = await svcInvoker.SendServiceRequest<TResponse>(paramsCarrier);
            return serviceResult.Value;
        }

        public static void RegisterServiceRequestType<T>() where T : ServiceRequest
        {
            var type = typeof(T);

            if (ServicesRequestTypes == null)
                ServicesRequestTypes = new Dictionary<string, Type>();

            ServicesRequestTypes.Add(type.FullName, type);
        }

    }
}

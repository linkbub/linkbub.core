﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Services.Operations;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace Common.Lib.Services.Pagination
{
    public class ResultsPaginationHandler
    {
        ConcurrentDictionary<Guid, PagingRequestInfo> CurrentRequests { get; set; }

        private int ExpirationMinutes { get; set; } = 1;

        private Timer RecycleTimer { get; set; }

        public ResultsPaginationHandler()
        {
            CurrentRequests = new ConcurrentDictionary<Guid, PagingRequestInfo>();

            // Create a timer with a ten second interval.
            RecycleTimer = new Timer(10000);

            RecycleTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            RecycleTimer.Enabled = true;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            var keysToRemove = new List<Guid>();
            foreach (var item in CurrentRequests)
            {
                if ((DateTime.Now - item.Value.LastRequestedOn).TotalMinutes > ExpirationMinutes)
                {
                    keysToRemove.Add(item.Key);
                }
            }

            foreach (var id in keysToRemove)
            {
                RemoveQueryInfo(id);
            }
        }

        public async Task<QueryResult> GetNextResults(QueryRepository queryRepository, IPersistanceRepository repository)
        {
            CurrentRequests.TryGetValue(queryRepository.QueryId, out PagingRequestInfo pageReqInfo);

            if (pageReqInfo != null)
            {
                pageReqInfo.LastRequestedOn = DateTime.Now;
            }
            else
            {
                QueryResult repoqr = null;
                switch(queryRepository.QueryType)
                {
                    case RepositoryQueryTypes.Where:
                        repoqr = await repository.WhereEntityAsync(queryRepository.ExpressionBuilder);
                        break;
                }
                var items = new List<Entity>();
                foreach (Entity item in repoqr.ObjectValue as IList)
                {
                    items.Add(item);
                }

                if (items.Count <= queryRepository.PageSize)
                {
                    repoqr.AvailablePages = 1;
                    return repoqr;
                }
                else
                {
                    pageReqInfo = new PagingRequestInfo
                    {
                        CurrentPage = -1,
                        InitialQueryResults = items,
                        RepoQuery = queryRepository,
                        LastRequestedOn = DateTime.Now
                    };

                    CurrentRequests.TryAdd(queryRepository.QueryId, pageReqInfo);
                }
            }

            return pageReqInfo.GetNextResults();            
        }

        public void RemoveQueryInfo(Guid queryId)
        {
            if (CurrentRequests.TryRemove(queryId, out PagingRequestInfo info))
                info.Dispose();
        }
    }

    public class PagingRequestInfo : IDisposable
    {
        public object locker = new object();

        public int CurrentPage { get; set; }

        public DateTime LastRequestedOn { get; set; }

        public QueryRepository RepoQuery { get; set; }

        public List<Entity> InitialQueryResults { get; set; }        

        public PagingRequestInfo()
        {
            //Todo: handle getting wrong page
            //Todo: handle update of the source
        }

        public QueryResult GetNextResults()
        {
            lock (locker)
            {
                CurrentPage++;

                var output = new QueryResult
                {
                    IsSuccess = true,
                    ObjectValue = InitialQueryResults.Skip(RepoQuery.PageSize * CurrentPage).Take(RepoQuery.PageSize).ToList(),
                    AvailablePages = (int)Math.Ceiling((double)InitialQueryResults.Count / RepoQuery.PageSize)
                };

                return output;
            }
        }

        public QueryResult GetPageResults(int requestedPage)
        {
            CurrentPage = requestedPage;

            var output = new QueryResult
            {
                IsSuccess = true,
                ObjectValue = InitialQueryResults.Skip(RepoQuery.PageSize * CurrentPage).Take(RepoQuery.PageSize).ToList(),
                AvailablePages = (int)Math.Ceiling((double)InitialQueryResults.Count / RepoQuery.PageSize)
            };

            return output;
        }

        public void Dispose()
        {
            lock (locker)
            {
                InitialQueryResults = null;
                RepoQuery = null;
            }
        }
    }
}

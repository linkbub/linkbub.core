﻿using Common.Lib.Infrastructure.Log;
using Common.Lib.Services.Operations;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services
{
    public class ParamsCarrier
    {
        //[JsonProperty(PropertyName = "ri")]
        public string RequestorId { get; set; }

        //[JsonProperty(PropertyName = "tr")]
        public OperationInfo Trace { get; set; }

        //[JsonProperty(PropertyName = "ot")]
        public OperationTypes OperationType { get; set; }

        //[JsonProperty(PropertyName = "aue")]
        public AddOrUpdateSingleEntity AddOrUpdateSingleEntity { get; set; }
        
        //[JsonProperty(PropertyName = "de")]
        public DeleteSingleEntity DeleteSingleEntity { get; set; }

        //[JsonProperty(PropertyName = "uow")]
        public UnitOfWorkInfo UnitOfWorkInfo { get; set; }


        //[JsonProperty(PropertyName = "qr")]
        public QueryRepository QueryRepository { get; set; }


        //[JsonProperty(PropertyName = "sr")]
        public string ServiceRequestInputParams { get; set; }


        //[JsonProperty(PropertyName = "srt")]
        public string ServiceRequestType { get; set; }
    }
}

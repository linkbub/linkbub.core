﻿using Common.Lib.Core;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.Log;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services
{
    public class ServiceResponse
    {
        [JsonIgnore]
        public IDependencyContainer DepCon
        {
            get
            {
                return Entity.Context.DepCon;
            }
        }
        public string ServiceType { get; set; }

        public OperationInfo Trace { get; set; }
    }
}

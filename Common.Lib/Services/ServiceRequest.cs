﻿using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using System.Threading.Tasks;
using Common.Lib.Core;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Services
{
    public class ServiceRequest : IDependencyInjectable
    {
        [JsonIgnore]
        public IDependencyContainer DepCon { get; set; }

        public virtual async Task<QueryResult> Execute(OperationInfo trace = null)
        {
            return new QueryResult();
        }

        public async Task<TResponse> Send<TResponse>(OperationInfo trace = null) where TResponse : ServiceResponse
        {
            return await ServiceHandler.Request<TResponse>(this, trace);
        }
    }
}

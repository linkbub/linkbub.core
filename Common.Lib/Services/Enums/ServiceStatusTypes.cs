﻿namespace Common.Lib.Services
{
    public enum ServiceStatusTypes
    {
        Ok,
        ServerDown,
        NoInternet,
        Synchronizing,
        CompilationVersionObsolete
    }
}

﻿namespace Common.Lib.Services
{
    public enum OperationTypes
    {
        RegisterClient = 0,
        QueryRepository = 1,
        HandleEntity = 2,
        ServiceRequest = 3
    }
}

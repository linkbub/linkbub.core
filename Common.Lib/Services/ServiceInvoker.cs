﻿using Common.Lib.Core;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Serialization;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Common.Lib.Services
{
    public class ServiceInvoker
    {
        public static string Host { get; set; }

        //public static Func<ParamsCarrier paramsCarrier, Promise<QueryResult> promise) where T : class, new()

        public static Func<string, ParamsCarrier, Task<QueryResult>> PostAction { get; set; }


        public static Func<string, Task<QueryResult<object>>> GetImageAction { get; set; }

        HttpClient Http { get; set; }



        public ServiceInvoker()
        {
            Http = new HttpClient();
        }

        public async Task<QueryResult> Send(ParamsCarrier paramsCarrier)
        {
            var output = new QueryResult();
            var pr = await PostAction(Host + "api/CentralService", paramsCarrier);

            if (pr.IsSuccess)
            {
                if (pr.ObjectValue != null)
                {
                    pr.ObjectValue = bool.Parse(pr.ObjectValue.ToString()) == true;
                }

                return pr;                
            }
            else
            {
                return new QueryResult() { IsSuccess = false, ResultNum = pr.ResultNum };
            }

        }

        public async Task<QueryResult<T>> Send<T>(ParamsCarrier paramsCarrier)
        {
            if (paramsCarrier.Trace != null)
                paramsCarrier.Trace.AddStep($"Call post action in ServiceInvoker.Send<T> Expected Type:{typeof(T).Name}");

            var pr = await PostAction(Host + "api/CentralService", paramsCarrier);
            {
                if (pr.IsSuccess)
                {
                    if (pr.Trace != null)
                        pr.Trace.AddStep($"post action result was success in ServiceInvoker.Send<T>. Request Deserialize");

                    return new QueryResult<T>() { IsSuccess = true, Value = CommonSerializer.DeserializeAction<T>(pr.ObjectValue.ToString()), Translations = pr.Translations };
                }
                else
                {
                    if (pr.Trace != null)
                        pr.Trace.AddStep($"post action result was failed.");

                    return new QueryResult<T>() { IsSuccess = false, ResultNum = pr.ResultNum };
                }
            }

        }


        public async Task<QueryResult<T>> Send<T>(ParamsCarrier paramsCarrier, IQueryResultSerializer deserializer) where T : class, new()
        {
            if (paramsCarrier.Trace != null)
                paramsCarrier.Trace.AddStep($"Call post action in ServiceInvoker.Send<T> Expected Type:{typeof(T).Name}");

            var pr = await PostAction(Host + "api/CentralService", paramsCarrier);
            
            if (pr.IsSuccess)
            {
                if (pr.Trace != null)
                    pr.Trace.AddStep($"post action result was success in ServiceInvoker.Send<T>. Request Deserialize");

                var value = deserializer.DeserializeQueryResult<T>(pr.ObjectValue.ToString());
                return value;
            }
            else
            {
                if (pr.Trace != null)
                    pr.Trace.AddStep($"post action result was failed.");

                return new QueryResult<T>() { IsSuccess = false, ResultNum = pr.ResultNum };
            }
        }

        public async Task<QueryResult<T>> SendServiceRequest<T>(ParamsCarrier paramsCarrier) where T : ServiceResponse
        {
            var output = new QueryResult<T>();

            var pr = await PostAction(Host + "api/CentralService", paramsCarrier);
            
            if (pr.IsSuccess)
            {
                return new QueryResult<T>() { IsSuccess = true, Value = CommonSerializer.DeserializeAction<T>(pr.ObjectValue.ToString()) };
            }
            else
            {
                return new QueryResult<T>() { IsSuccess = false, ResultNum = pr.ResultNum, Value = null };
            }
        }
    }
}

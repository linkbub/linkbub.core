﻿using System;
using System.Collections.Generic;
using Common.Lib.Core;
using Common.Lib.Core.Context;

namespace Common.Lib.Infrastructure
{
    public class DependencyContainer : IDependencyContainer
    {
        public Dictionary<Type, Func<object>> EmptyConstructors { get; set; }

        //public Dictionary<Type, Func<IUnitOfWork, IRepository>> RepositoriesConstructors { get; set; }
        Dictionary<Type, object> RepositoriesConstructors { get; set; }

        //Dictionary<string, Func<IUnitOfWork, IRepository>> RepositoriesConstructorsByName { get; set; }
        Dictionary<string, object> RepositoriesConstructorsByName { get; set; }
        
        Dictionary<Type, object> Singletons { get; set; }

        Dictionary<string, Type> RegisteredTypes { get; set; }

        public DependencyContainer()
        {
            EmptyConstructors = new Dictionary<Type, Func<object>>();
            //RepositoriesConstructors = new Dictionary<Type, Func<IUnitOfWork, IRepository>>();
            RepositoriesConstructors = new Dictionary<Type, object>();
            //RepositoriesConstructorsByName = new Dictionary<string, Func<IUnitOfWork, IRepository>>();
            RepositoriesConstructorsByName = new Dictionary<string, object>();
            Singletons = new Dictionary<Type, object>();

            RegisteredTypes = new Dictionary<string, Type>();
        }

        public bool RegisterConstructor<T>(Func<T> constructor) where T : IDependencyInjectable
        {
            var t = typeof(T);
            if (EmptyConstructors.ContainsKey(t))
                EmptyConstructors[t] = () => { return constructor(); };
            else
                EmptyConstructors.Add(t, () => { return constructor(); });

            return true;
        }

        public bool RegisterRepository<T>(Func<IUnitOfWork, IRepository<T>> constructor) where T : Entity, new()
        {
            var t = typeof(T);
            if (RepositoriesConstructors.ContainsKey(t))
            {
                RepositoriesConstructors[t] = constructor;
                RepositoriesConstructorsByName[t.FullName] = constructor;
            }
            else
            {
                RepositoriesConstructors.Add(t, constructor);
                RepositoriesConstructorsByName.Add(t.FullName, constructor);
            }

            return true;
        }

        public T ResolveAndConstruct<T>() where T : IDependencyInjectable
        {
            var t = typeof(T);

            T output = default(T);

            if (EmptyConstructors.ContainsKey(t))
                output = (T)EmptyConstructors[t]();
            else
                output = default(T);

            output.DepCon = this;

            return output;
        }

        public IRepository<T> ResolveRepository<T>(IUnitOfWork uow = null) where T : Entity, new()
        {
            var t = typeof(T);

            if (RepositoriesConstructors.ContainsKey(t))
            {
                var ctor = (Func<IUnitOfWork, IRepository<T>>)RepositoriesConstructors[t];
                var output = ctor(uow) as IRepository<T>;
                output.DepCon = this;

                return output;
            }
            else
            {
                throw new Exception("Repository for " + t.Name + " is not registered");
            }
        }

        public IRepository ResolveRepository(string typeName, IUnitOfWork uow = null)
        {
            if (RepositoriesConstructorsByName.ContainsKey(typeName))
            {
                var ctor = (Func<IUnitOfWork, IRepository>)RepositoriesConstructorsByName[typeName];
                var output = ctor(uow);
                output.DepCon = this;
                
                return output;
            }
            else
            {
                throw new Exception("Repository for " + typeName + " is not registered");
            }
        }

        public T RegisterSingleton<T>(T instance)
        {
            if (Singletons.ContainsKey(typeof(T)))
                Singletons[typeof(T)] = instance;
            else
                Singletons.Add(typeof(T), instance);

            return instance;
        }

        public T ResolveSingleton<T>(Func<T> instanceBuilder = null)
        {
            if (Singletons.ContainsKey(typeof(T)))
                return (T)Singletons[typeof(T)];
            else
            {
                return instanceBuilder == null ? default(T) : RegisterSingleton<T>(instanceBuilder());
            }
        }

        public bool RegisterType<T>()
        {
            var t = typeof(T);

            return RegisterType(t);
        }

        public bool RegisterType(Type t)
        {

            if (!RegisteredTypes.ContainsKey(t.FullName))
            {
                RegisteredTypes.Add(t.FullName, t);
                return true;
            }
            return false;
        }

        public Type ResolveType(string typeName)
        {
            if (RegisteredTypes.ContainsKey(typeName))
            {
                return RegisteredTypes[typeName];
            }
            return null;
        }
    }
}

﻿using System;

namespace Common.Lib.Infrastructure.Log
{
    public class ActionInfo
    {
        public string UserId { get; set; }

        public string DeviceId { get; set; }

        public DateTime ActionDate { get; set; }
    }
}

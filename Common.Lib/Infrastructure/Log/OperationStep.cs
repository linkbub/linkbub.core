﻿using System;

namespace Common.Lib.Infrastructure.Log
{
    public class OperationStep
    {
        public string Action { get; set; }

        public string Context { get; set; }

        public DateTime Time { get; set; }

        public bool IsSucces { get; set; }

        public string Comments { get; set; }

        public bool IsServerSide { get; set; }
    }
}

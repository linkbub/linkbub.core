﻿using System;
using System.Collections.Generic;

namespace Common
{
    public static class CoreLog
    {
        private static List<string> MethodFilters = new List<string>();

        public static void AddFilterMethod(string method)
        {
            if (!MethodFilters.Contains(method))
            {
                MethodFilters.Add(method);
            }
        }

        public static bool IsTraceActive { get; set; }

        private static object _DeviceLogActionLocker = new Object();
        public static Action<string> DeviceLogAction;

        private static DateTime LastActionTime { get; set; }

        public static void Trace(string callerClass, string callerMethod, string message)
        {
            if (IsTraceActive)
            {
                lock (_DeviceLogActionLocker)
                {
                    if (DeviceLogAction != null)
                    {
                        if (MethodFilters.Count == 0 || MethodFilters.Contains(callerMethod))
                        {
                            var currentTime = DateTime.Now;
                            var deltaTime = LastActionTime == default(DateTime) ? 0 : (currentTime - LastActionTime).TotalMilliseconds;
                            LastActionTime = currentTime;

                            DeviceLogAction("Caller[" + callerClass + "." + callerMethod + "] (" + deltaTime + "ms)" + message);
                        }
                    }
                }
            }
        }
    }
}

﻿using Common.Lib.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Lib.Infrastructure.Log
{
    public class OperationInfo
    {
        public string OperationId { get; set; }
        public string Name { get; set; }

        public string ParentOperationId { get; set; }

        public string UserId { get; set; }

        public string ClientDeviceId { get; set; }

        public DateTime StartedOn { get; set; }

        public List<OperationStep> Steps { get; set; }


        public OperationInfo()
        {
            Steps = new List<OperationStep>();
        }

        public void AddStep(string action, bool isServerSeide = false, string comments = null, bool isSuccess = true)
        {
            Steps.Add(new OperationStep()
            {
                Action = action,
                Comments = comments,
                Time = DateTime.Now,
                IsSucces = isSuccess,
                IsServerSide = isServerSeide
            });
        }

        public static OperationInfo FromString(string input)
        {
            var output = CommonSerializer.DeserializeEncoded<OperationInfo>(input);
            return output;
        }

        public static OperationInfo ClientNew(string clientDeviceId = null)
        {
            var output = new OperationInfo
            {
                OperationId = Guid.NewGuid().ToString(),
                StartedOn = DateTime.Now
            };

            if (clientDeviceId != null)
                output.ClientDeviceId = clientDeviceId;

            return output;
        }

        public string Export()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Operation: {Name} Id: {OperationId}");

            var previousTime = StartedOn;
            var totalTime = 0.0;
            foreach (var step in Steps)
            {
                var side = step.IsServerSide ? "Server" : "Client";
                var delta = Math.Round((step.Time - previousTime).TotalMilliseconds, 2);
                totalTime = Math.Round((step.Time - StartedOn).TotalMilliseconds, 2);
                sb.AppendLine($"   - {step.Action}");
                sb.AppendLine($"   [{side}] delta: {delta}ms total:{totalTime}ms");
                previousTime = step.Time;
            }

            return sb.ToString();
        }

    }
}

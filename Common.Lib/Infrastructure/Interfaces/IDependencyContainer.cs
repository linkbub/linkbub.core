﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using System;

namespace Common.Lib.Infrastructure
{
    public interface IDependencyContainer
    {
        bool RegisterType<T>();
        bool RegisterType(Type t);
        Type ResolveType(string typeName);            

        bool RegisterConstructor<T>(Func<T> constructor) where T : IDependencyInjectable;

        T ResolveAndConstruct<T>() where T : IDependencyInjectable;

        bool RegisterRepository<T>(Func<IUnitOfWork, IRepository<T>> constructor) where T : Entity, new();

        IRepository<T> ResolveRepository<T>(IUnitOfWork uow = null) where T : Entity, new();

        IRepository ResolveRepository(string typeName, IUnitOfWork uow = null);

        T RegisterSingleton<T>(T instance);

        T ResolveSingleton<T>(Func<T> instanceBuilder = null);
    }
}

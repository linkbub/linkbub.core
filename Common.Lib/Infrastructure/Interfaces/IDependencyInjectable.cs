﻿namespace Common.Lib.Infrastructure
{
#if NETJS
    [Bridge.Reflectable]
#endif
    public interface IDependencyInjectable
    {
        IDependencyContainer DepCon { get; set; }
    }
}

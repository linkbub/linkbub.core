﻿using System.Collections.Generic;

namespace Common.Lib.Infrastructure.ActionsResults
{
    public class ValidationResult : ActionResult
    {
    }

    public class ValidationResult<T> : ValidationResult
    {
        public T ValidatedResult { get; set; }
    }
}

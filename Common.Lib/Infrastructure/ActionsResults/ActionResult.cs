﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Lib.Infrastructure.ActionsResults
{
    public class ActionResult
    {
        public virtual bool IsSuccess { get; set; }
        public List<string> Errors { get; set; } = new List<string>();

        public string AllErrors
        {
            get
            {
                var output = string.Empty;

                foreach (var error in Errors)
                    output += error + "\n\r";

                return output;
            }
        }
    }
}

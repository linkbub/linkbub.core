﻿using Common.Lib.Babylon;
using Common.Lib.Core;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Serialization;
using Common.Lib.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Infrastructure.ActionsResults
{
    public class QueryResult : ActionResult
    {
        [JsonIgnore]
        public static Func<object, object> ToArrayAction { get; set; }

        public object ObjectValue { get; set; }

        [JsonIgnore]
        public bool ValueAsBool
        {
            get
            {
                if (ObjectValue is bool)
                    return (bool)ObjectValue;
                if (ObjectValue == null)
                    return this.IsSuccess;
                else if (ObjectValue.ToString() == "true")
                    return true;
                else
                    return false;

            }
        }

        public int ResultNum { get; set; }
        
        public OperationInfo Trace { get; set; }

        public string Message { get; set; }
        
        public List<EntityTranslation> Translations { get; set; }

        public ServiceStatusTypes ServiceStatus { get; set; }

        /// <summary>
        /// When a collection is requested in a paged mode
        /// the query result indicates the amount of pages available
        /// </summary>
        public int AvailablePages { get; set; }

        HashSet<string> TypesWithIncludeChildren { get; set; } = new HashSet<string>();

        [JsonIgnore]
        public Dictionary<string, IList> MultiResultEntities
        {
            get
            {
                return _multiResultEntities;
            }
            set
            {
                _multiResultEntities = value;
            }
        }
        Dictionary<string, IList> _multiResultEntities;

        HashSet<Guid> AddedIds { get; set; } = new HashSet<Guid>();

        public string Serialize(IDependencyContainer dc)
        {
            using (var serializer = dc.ResolveAndConstruct<IQueryResultSerializer>())
            {
                serializer.AddQueryResult(this);
                return serializer.ToStringEncoded();
            }
        }

        public void AddMultiResultEntities(string type, IList entities)
        {
            if (MultiResultEntities == null)
                MultiResultEntities = new Dictionary<string, IList>();

            if (!MultiResultEntities.ContainsKey(type))
                MultiResultEntities.Add(type, new List<Entity>());

            foreach (var e in entities.Cast<Entity>())
            {
                if (!AddedIds.Contains(e.Id))
                {
                    MultiResultEntities[type].Add(e);
                    AddedIds.AddMissing(e.Id);
                }
            }
        }

        public void AddMultiResultEntity<T>(string type, T e) where T : Entity
        {
            if (MultiResultEntities == null)
                MultiResultEntities = new Dictionary<string, IList>();

            if (!MultiResultEntities.ContainsKey(type))
                MultiResultEntities.Add(type, new List<Entity>());

            if (!AddedIds.Contains(e.Id))
            {
                MultiResultEntities[type].Add(e);
                AddedIds.AddMissing(e.Id);
            }
        }

        public void AddTypeToIncludedChildren(string typeName)
        {
            if (!TypesWithIncludeChildren.Contains(typeName))
                TypesWithIncludeChildren.Add(typeName);
        }

        public bool TypeIncludeChildren(string typeName)
        {
            return TypesWithIncludeChildren.Contains(typeName);
        }
    }

    public class QueryResult<T> : QueryResult// where T : class
    {
        public T Value
        {
            get
            {
                return (T)ObjectValue;
            }
            set
            {
                ObjectValue = value;
            }
        }

        public object ToArray()
        {
            return ToArrayAction(this.Value);
        }

        public QueryResult<TCast> CastTo<TCast>()
        {
            return new QueryResult<TCast>
            {
                ObjectValue = this.Value,
                ResultNum = this.ResultNum,
                Trace = this.Trace,
                Message = this.Message,
                Translations = this.Translations,
                ServiceStatus = this.ServiceStatus,
                AvailablePages = this.AvailablePages,
                MultiResultEntities = this.MultiResultEntities
            };
        }
    }
}

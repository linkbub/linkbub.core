﻿using Common.Lib.Serialization;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Common.Lib.Cryptography
{
    public class Tools
    {
        public static string GetSHA256Hash(byte[] bytes)
        {
            var hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = CommonSerializer.EncodeTo64(hash);

            return hashString;
        }

        public static string GetSHA256Hash(string input)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(input);
            var hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = CommonSerializer.EncodeTo64(hash);

            return hashString;
        }
    }
}

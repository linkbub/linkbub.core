﻿using System.Collections;
using System.Collections.Generic;

namespace Common.Lib.Core
{
    public class SimpleTypeList<T> : ComplexType, ICollection<T>
    {
        List<T> Items = new List<T>();

        public int Count
        {
            get
            {
                return Items.Count;
            }
        }

        public bool IsReadOnly => false;

        public void Add(T item)
        {
            if (!Contains(item))
            {
                Items.Add(item);
            }
        }

        public void Clear()
        {
            Items.Clear();
        }

        public bool Contains(T item)
        {
            return Items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Items.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            if (Contains(item))
            {
                var output = Items.Remove(item);
                return output;
            }
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Items.GetEnumerator();
        }

    }
}

﻿using System;
using System.Linq.Expressions;

namespace Common.Lib.Core
{
    public abstract class BaseExpression : IExpressionBuilder
    {
        public abstract Func<TEntity, bool> Build<TEntity>() where TEntity : Entity;

        public virtual Func<T, TKey> Build<T, TKey>() where T : Entity
        {
            if (typeof(TKey) == typeof(bool))
            {
                return Build<T>() as Func<T, TKey>;
            }
            throw new NotImplementedException();
        }

        public abstract Expression<Func<TEntity, bool>> CreateExpression<TEntity>() where TEntity : Entity;

        public virtual Expression<Func<T, TKey>> CreateExpression<T, TKey>() where T : Entity
        {
            if (typeof(TKey) == typeof(bool))
            {
                return CreateExpression<T>() as Expression<Func<T, TKey>>;
            }
            throw new NotImplementedException();
        }

        public abstract object[] GetParameters();

        public abstract void SetParameters(object[] parameters);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Common.Lib.Core
{
    public class Entity_Id_in_values : BaseExpression
    {
        public HashSet<Guid> Ids { get; set; }

        public override Func<TEntity, bool> Build<TEntity>()
        {
            return (x) => Ids.Contains(x.Id);
        }

        public override Expression<Func<TEntity, bool>> CreateExpression<TEntity>()
        {
            return (x) => Ids.Contains(x.Id);
        }

        public override object[] GetParameters()
        {
            return new object[] { Ids.ToList() };
        }

        public override void SetParameters(object[] parameters)
        {
            if (parameters[0] is List<Guid>)
            {
                Ids = new HashSet<Guid>((List<Guid>)parameters[0]);
            }
            else
            {
                var ids = new List<Guid>();
                foreach (var o in parameters)
                {
                    ids.Add(new Guid(o.ToString()));
                }
                Ids = new HashSet<Guid>(ids);
            }
        }

        public static Entity_Id_in_values Create(List<Guid> ids)
        {
            return new Entity_Id_in_values() { Ids = new HashSet<Guid>(ids) };
        }

        public static Entity_Id_in_values Create(params Guid[] ids)
        {
            return new Entity_Id_in_values() { Ids = new HashSet<Guid>(ids) };
        }
    }
}

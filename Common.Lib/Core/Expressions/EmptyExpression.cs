﻿using System;
using System.Linq.Expressions;

namespace Common.Lib.Core
{
    public class EmptyExpression : BaseExpression
    {
        public override Func<TEntity, bool> Build<TEntity>()
        {
            return (x) => true;
        }

        public override Expression<Func<TEntity, bool>> CreateExpression<TEntity>()
        {
            return (x) => true;
        }

        public override object[] GetParameters()
        {
            return new object[] {  };
        }

        public override void SetParameters(object[] parameters)
        {
        }

        public static EmptyExpression Create()
        {
            return new EmptyExpression();
        }
    }
}

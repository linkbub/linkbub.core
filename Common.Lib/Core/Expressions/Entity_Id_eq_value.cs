﻿using System;
using System.Linq.Expressions;

namespace Common.Lib.Core
{
    public class Entity_Id_eq_value : BaseExpression
    {
        public Guid Id { get; set; }

        public override Func<TEntity, bool> Build<TEntity>()
        {
            return (x) => x.Id == Id;
        }

        public override Expression<Func<TEntity, bool>> CreateExpression<TEntity>()
        {
            return (x) => x.Id == Id;
        }

        public override object[] GetParameters()
        {
            return new object[] { Id };
        }

        public override void SetParameters(object[] parameters)
        {

            Id = new Guid(parameters[0].ToString());
        }

        public static Entity_Id_eq_value Create(Guid id)
        {
            return new Entity_Id_eq_value() { Id = id };
        }
    }
}

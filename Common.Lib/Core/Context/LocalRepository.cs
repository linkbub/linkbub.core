﻿using Common.Lib.Babylon;
using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public class LocalRepository<T> : IRepository<T>, IPersistanceRepository<T> where T : Entity, new()
    {
        public bool PopulateBabylonConcepts { get; set; }
        public int SelectedLanguage { get; set; } = 1;

        public IDbSet<T> DbSet
        {
            get
            {
                return _dbSet ?? (_dbSet = DepCon.ResolveAndConstruct<IDbSet<T>>());
            }
        }
        IDbSet<T> _dbSet;

        public IDependencyContainer DepCon { get; set; }

        public IUnitOfWork UoW { get; set; }

        public LocalRepository()
        {
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return DbSet.GetEnumerator();
        }



        #region IRepository
        ////public async Task<QueryResult<Entity>> FindEntityAsync(Guid id, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        ////{
        ////    return await FindAsync(id);
        ////}

        ////public async Task<QueryResult<Entity>> AddEntityAsync(Entity entity)
        ////{
        ////    return (await AddAsync(entity as T)).CastTo<Entity>();
        ////}

        ////public async Task<QueryResult<Entity>> UpdateEntityAsync(Entity entity)
        ////{
        ////    return (await UpdateAsync(entity as T)).CastTo<Entity>();
        ////}
        ////public async Task<QueryResult> DeleteEntityAsync(Entity entity)
        ////{
        ////    return await DeleteAsync(entity as T);
        ////}

        ////public async Task<QueryResult> DeleteEntityAsync(Guid id)
        ////{
        ////    return await DeleteAsync(id);
        ////}

        ////public async Task<QueryResult> WhereEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        ////{
        ////    var queryResult = new QueryResult<List<T>>();
        ////    var expression = condition.Build<T>();

        ////    // todo: handle WhereAsync possible error
        ////    queryResult.Value = (await DbSet.WhereAsync(condition)).Value;
        ////    queryResult.Translations = new List<EntityTranslation>();

        ////    foreach (var entity in queryResult.Value)
        ////    {
        ////        if (addedIds == null)
        ////            addedIds = new HashSet<Guid>();

        ////        if (PopulateBabylonConcepts)
        ////            entity.IncludeBabylonConcepts();
        ////        else
        ////            entity.ExcludeBabylonConcepts();

        ////        if (childrenPolicyType != ChildrenPolicyTypes.None)
        ////            entity.IncludeChildren((int)childrenPolicyType, PopulateBabylonConcepts, addedIds);
        ////        else
        ////            entity.ExcludeChildren();

        ////        // todo: review this, it seems it is doing nothing but maybe it is populating somehting relating to translations
        ////        queryResult
        ////            .Translations
        ////            .AddRange(entity.ObtainTranslations(SelectedLanguage)
        ////            .Select(x => new EntityTranslation()
        ////            {
        ////                EntityId = entity.Id,
        ////                PropertyId = x.Key,
        ////                Text = x.Value
        ////            }));
        ////    }

        ////    queryResult.IsSuccess = true;

        ////    return queryResult;
        ////}

        ////public async Task<QueryResult> FirstOrDefaultEntityAsync(IExpressionBuilder condition, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        ////{
        ////    var queryResult = new QueryResult<T>();
        ////    //var expression = condition.Build<T>();

        ////    queryResult.Value = (await DbSet.FirstOrDefaultAsync(condition)).Value;

        ////    if (queryResult.Value != null)
        ////    {
        ////        if (addedIds == null)
        ////            addedIds = new HashSet<Guid>();

        ////        if (PopulateBabylonConcepts)
        ////            queryResult.Value.IncludeBabylonConcepts();
        ////        else
        ////            queryResult.Value.ExcludeBabylonConcepts();

        ////        if (childrenPolicyType != ChildrenPolicyTypes.None)
        ////            queryResult.Value.IncludeChildren((int)childrenPolicyType, PopulateBabylonConcepts, addedIds);
        ////        else
        ////            queryResult.Value.ExcludeChildren();

        ////        queryResult.Translations = new List<EntityTranslation>();
        ////        queryResult
        ////                 .Translations
        ////                 .AddRange(queryResult.Value.ObtainTranslations(SelectedLanguage)
        ////                 .Select(x => new EntityTranslation()
        ////                 {
        ////                     EntityId = queryResult.Value.Id,
        ////                     PropertyId = x.Key,
        ////                     Text = x.Value
        ////                 }));
        ////    }

        ////    queryResult.IsSuccess = true;

        ////    return queryResult;
        ////}

        ////public async Task<QueryResult> AnyEntityAsync(IExpressionBuilder condition)
        ////{
        ////    //var queryResult = new QueryResult<T>();
        ////    //var expression = condition.Build<T>();

        ////    return await DbSet.AnyAsync(condition);

        ////    //return queryResult;
        ////}

        ////public async Task<QueryResult> FindEntityAsync(IExpressionBuilder condition, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        ////{
        ////    var queryResult = new QueryResult<T>();
        ////    //var expression = condition.Build<T>();

        ////    queryResult.Value = (await DbSet.FindAsync((condition as Entity_Id_eq_value).Id)).Value;

        ////    if (addedIds == null)
        ////        addedIds = new HashSet<Guid>();

        ////    if (PopulateBabylonConcepts)
        ////        queryResult.Value.IncludeBabylonConcepts();
        ////    else
        ////        queryResult.Value.ExcludeBabylonConcepts();

        ////    if (childrenPolicyType != ChildrenPolicyTypes.None)
        ////        queryResult.Value.IncludeChildren((int)childrenPolicyType, PopulateBabylonConcepts, addedIds);
        ////    else
        ////        queryResult.Value.ExcludeChildren();

        ////    queryResult.Translations = new List<EntityTranslation>();
        ////    queryResult
        ////             .Translations
        ////             .AddRange(queryResult.Value.ObtainTranslations(SelectedLanguage)
        ////             .Select(x => new EntityTranslation()
        ////             {
        ////                 EntityId = queryResult.Value.Id,
        ////                 PropertyId = x.Key,
        ////                 Text = x.Value
        ////             }));
        ////    queryResult.IsSuccess = true;

        ////    return queryResult;
        ////}

        ////public async Task<QueryResult> CountEntityAsync(IExpressionBuilder condition)
        ////{
        ////    //var queryResult = new QueryResult<int>();
        ////    //var expression = condition.Build<T>();

        ////    //queryResult.Value = DbSet.Count(expression);
        ////    //queryResult.IsSuccess = true;

        ////    //return queryResult;
        ////    return await DbSet.CountAsync(condition);
        ////}

        ////public async Task<QueryResult> GetAllEntityAsync(ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        ////{
        ////    var queryResult = new QueryResult<List<T>>();

        ////    queryResult.Value = await DbSet.ToListAsync();
        ////    queryResult.Translations = new List<EntityTranslation>();

        ////    foreach (var entity in queryResult.Value)
        ////    {
        ////        if (addedIds == null)
        ////            addedIds = new HashSet<Guid>();

        ////        if (PopulateBabylonConcepts)
        ////            entity.IncludeBabylonConcepts();
        ////        else
        ////            entity.ExcludeBabylonConcepts();

        ////        if (childrenPolicyType != ChildrenPolicyTypes.None)
        ////            entity.IncludeChildren((int)childrenPolicyType, PopulateBabylonConcepts, addedIds);
        ////        else
        ////            entity.ExcludeChildren();

        ////        queryResult
        ////            .Translations
        ////            .AddRange(entity.ObtainTranslations(SelectedLanguage)
        ////            .Select(x => new EntityTranslation()
        ////            {
        ////                EntityId = entity.Id,
        ////                PropertyId = x.Key,
        ////                Text = x.Value
        ////            }));
        ////    }

        ////    queryResult.IsSuccess = true;

        ////    return queryResult;
        ////}
        #endregion

        #region IServerRepository
        public async Task<QueryResult<Entity>> FindEntityAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return (await FindAsync(id, actionInfo, trace, childrenPolicyType)).CastTo<Entity>();
        }

        public async Task<QueryResult<Entity>> AddEntityAsync(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return (await AddAsync(entity as T, actionInfo, trace, childrenPolicyType)).CastTo<Entity>();
        }

        public async Task<QueryResult<Entity>> UpdateEntityAsync(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return (await UpdateAsync(entity as T, actionInfo, trace, childrenPolicyType)).CastTo<Entity>();
        }

        public async Task<QueryResult> DeleteEntityAsync(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DeleteAsync(entity as T, actionInfo, trace);
        }

        public async Task<QueryResult> DeleteEntityAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DeleteAsync(id, actionInfo, trace);
        }

        public async Task<QueryResult> GetAllEntityAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await GetAllAsync(actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult> WhereEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await WhereAsync(condition, actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult> FirstOrDefaultEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await FirstOrDefaultAsync(condition, actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult> AnyEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await AnyAsync(condition, actionInfo, trace);
        }

        public async Task<QueryResult<int>> CountEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await CountAsync(condition, actionInfo, trace);
        }
        #endregion

        public async Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            if (!AddInternal(entity))
                return new QueryResult<T>() { IsSuccess = false, ResultNum = (int)SaveEntityResultsTypes.DbAddError };

            return await DbSet.AddAsync(entity);

        }

        bool AddInternal(T entity)
        {
            if (entity.Id == default(Guid))
                entity.Id = Guid.NewGuid();

            entity.CreatedOn = DateTime.Now;
            entity.UpdatedOn = DateTime.Now;

            return true;
        }               

        public async Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            if (!UpdateInternal(entity))
            {
                return new QueryResult<T>() { IsSuccess = false, ResultNum = (int)SaveEntityResultsTypes.DbUpdateError };
            }
            else
            {
                var output = await DbSet.UpdateAsync(entity);
                output.ResultNum = (int)SaveEntityResultsTypes.Ok;
                return output;
            }
        }

        bool UpdateInternal(T entity)
        {
            if (entity.Id == default(Guid))
                return false;

            entity.UpdatedOn = DateTime.Now;
            return true;
        }


        public IEnumerator<T> GetEnumerator()
        {
            return DbSet.GetEnumerator();
        }

        public async Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DbSet.DeleteAsync(entity);
        }

        public async Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DbSet.DeleteAsync(id);
        }
        
        public async Task<List<T>> ToListAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await DbSet.ToListAsync();
        }

        public async Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await FindAsyncInternal(id, actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await FirstOrDefaultAsyncInternal(condition, actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await GetAllAsyncInternal(actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await WhereAsyncInternal(condition, actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DbSet.AnyAsync(condition);
        }

        public async Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DbSet.CountAsync(condition);
        }

        #region private

        private async Task IncludeChildrenAndTranslations(ChildrenPolicyTypes childrenPolicyType, QueryResult<T> queryResult)
        {
            if (PopulateBabylonConcepts)
                await queryResult.Value.IncludeBabylonConcepts();
            else
                queryResult.Value.ExcludeBabylonConcepts();

            if (childrenPolicyType != ChildrenPolicyTypes.None)
                await queryResult.Value.IncludeChildren((int)childrenPolicyType, PopulateBabylonConcepts, queryResult);
            else
                queryResult.Value.ExcludeChildren();



            queryResult.Translations = new List<EntityTranslation>();
            queryResult
                     .Translations
                     .AddRange(queryResult.Value.ObtainTranslations(SelectedLanguage)
                     .Select(x => new EntityTranslation()
                     {
                         EntityId = queryResult.Value.Id,
                         PropertyId = x.Key,
                         Text = x.Value
                     }));
            queryResult.IsSuccess = true;
        }

        private async Task IncludeChildrenAndTranslations(ChildrenPolicyTypes childrenPolicyType, QueryResult<List<T>> queryResult)
        {
            queryResult.Translations = new List<EntityTranslation>();

            foreach (var entity in queryResult.Value)
            {
                if (PopulateBabylonConcepts)
                    await entity.IncludeBabylonConcepts();
                else
                    entity.ExcludeBabylonConcepts();

                if (childrenPolicyType != ChildrenPolicyTypes.None)
                    await entity.IncludeChildren((int)childrenPolicyType, PopulateBabylonConcepts, queryResult);
                else
                    entity.ExcludeChildren();

                // todo: review this, it seems it is doing nothing but maybe it is populating somehting relating to translations
                queryResult
                    .Translations
                    .AddRange(entity.ObtainTranslations(SelectedLanguage)
                    .Select(x => new EntityTranslation()
                    {
                        EntityId = entity.Id,
                        PropertyId = x.Key,
                        Text = x.Value
                    }));
            }
        }

        private async Task<QueryResult<T>> FindAsyncInternal(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            var output = new QueryResult<T>();

            output.Value = (await DbSet.FindAsync(id)).Value;

            if (output.Value != null)
                await IncludeChildrenAndTranslations(childrenPolicyType, output);

            return output;
        }

        private async Task<QueryResult<T>> FirstOrDefaultAsyncInternal(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            var output = new QueryResult<T>();

            output.Value = (await DbSet.FirstOrDefaultAsync(condition)).Value;

            if (output.Value != null)
                await IncludeChildrenAndTranslations(childrenPolicyType, output);

            return output;
        }

        private async Task<QueryResult<List<T>>> GetAllAsyncInternal(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await WhereAsyncInternal(new EmptyExpression(), actionInfo, trace, childrenPolicyType);
        }

        private async Task<QueryResult<List<T>>> WhereAsyncInternal(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            var output = new QueryResult<List<T>>();

            // todo: handle WhereAsync possible error
            output.Value = (await DbSet.WhereAsync(condition)).Value;
            await IncludeChildrenAndTranslations(childrenPolicyType, output);

            output.IsSuccess = true;

            return output;
        }


        #endregion


        public void Dispose()
        {
            DbSet.Dispose();
        }        
    }
}

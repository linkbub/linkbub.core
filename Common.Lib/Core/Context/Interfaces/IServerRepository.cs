﻿using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public interface IPersistanceRepository : IRepository
    {
        Task<QueryResult<Entity>> FindEntityAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);
        Task<QueryResult<Entity>> AddEntityAsync(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);
        Task<QueryResult<Entity>> UpdateEntityAsync(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);
        Task<QueryResult> DeleteEntityAsync(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null);
        Task<QueryResult> DeleteEntityAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult> GetAllEntityAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);
        Task<QueryResult> WhereEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult> FirstOrDefaultEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult> AnyEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<int>> CountEntityAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);
    }


    public interface IPersistanceRepository<T> : IPersistanceRepository where T : Entity, new()
    {
    }
}

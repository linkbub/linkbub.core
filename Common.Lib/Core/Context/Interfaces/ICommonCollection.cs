﻿using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public interface ICommonCollection<T> : IEnumerable<T> where T : Entity
    {
        Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

        Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);
        Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<List<T>> ToListAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);
    }

}

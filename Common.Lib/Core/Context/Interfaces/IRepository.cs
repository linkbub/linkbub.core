﻿using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public interface IRepository : IDependencyInjectable, IDisposable
    {
        int SelectedLanguage { get; set; }

        bool PopulateBabylonConcepts { get; set; }
    }


    public interface IRepository<T> : IRepository, ICommonCollection<T> where T : Entity, new()
    {
        
    }
}

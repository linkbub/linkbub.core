﻿using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public interface IClientRepository<T> : IRepository<T> where T : Entity, new()
    {
        bool QueryMustGiveResult { get; set; }
        bool ForceServerCall { get; set; }

        IRepository<T> GetNextPage(int pageSize = 20);
        IRepository<T> GetPage(int pageNum, int pageSize = 20);

        /// <summary>
        /// Returns the query paged and the page 0
        /// </summary>
        /// <param name="pageSize">Page Size</param>
        /// <returns>The repository to keep asking fluently</returns>
        IRepository<T> GetPaged(int pageSize = 20);

        Task<QueryResult<int>> CountAsync(ActionInfo actionInfo = null, OperationInfo trace = null);
        Task<QueryResult> AnyAsync(ActionInfo actionInfo = null, OperationInfo trace = null);
        Task<QueryResult<T>> FirstOrDefaultAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None);

    }
}

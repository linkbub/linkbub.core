﻿using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public interface IUnitOfWork : IDependencyInjectable, IDisposable
    {
        List<UoWActInfo> UowActions { get; set; }

        bool AddEntitySave(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null);
        QueryResult Commit(ActionInfo actionInfo = null, OperationInfo trace = null);
        Task<QueryResult> CommitAsync(ActionInfo actionInfo = null, OperationInfo trace = null);
    }
}

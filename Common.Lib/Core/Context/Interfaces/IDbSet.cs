﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;

namespace Common.Lib.Core.Context
{
    public interface IDbSet : IDisposable
    {
        Task<QueryResult<Entity>> FindObjectAsync(Guid id);
        Task<QueryResult<Entity>> AddObjectAsync(Entity entity);
        Task<QueryResult<Entity>> UpdateObjectAsync(Entity entity);
    }

    public interface IDbSet<T> : IDbSet, IEnumerable<T>, IDependencyInjectable where T : Entity
    {
        Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);
        Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null);

        Task<List<T>> ToListAsync();
    }
}

﻿using Common.Lib.Infrastructure;

namespace Common.Lib.Core.Context
{
    public interface IContext : IDependencyInjectable
    {
        ContextModesTypes ContextMode { get; set; }

        IUnitOfWork GetUnitOfWork();
    }
}

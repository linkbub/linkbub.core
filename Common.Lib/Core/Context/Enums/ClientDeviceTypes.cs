﻿namespace Common.Lib.Core.Context
{
    public enum ClientDeviceTypes
    {
        Android,
        iOS,
        Windows,
        WebGL,
        Web
    }
}

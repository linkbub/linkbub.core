﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;

namespace Common.Lib.Core.Context
{
    public class MemUnitOfWork : IUnitOfWork
    {
        public IDependencyContainer DepCon { get; set; }

        public List<UoWActInfo> UowActions { get; set; }

        public MemUnitOfWork()
        {
            UowActions = new List<UoWActInfo>();
        }

        public bool AddEntitySave(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            UowActions.Add(new UoWActInfo()
            {
                EntityChange = entity.GetChanges(),
                UserId = actionInfo == null ? string.Empty : actionInfo.UserId,
                DeviceId = actionInfo == null ? string.Empty : actionInfo.DeviceId,
                ActionTime = actionInfo == null ? DateTime.Now : actionInfo.ActionDate,
                ActionInfoType = ActionInfoTypes.Save
            });
            return true;
        }

        public QueryResult Commit(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return CommitAsync(actionInfo, trace).Result;
        }

        public async Task<QueryResult> CommitAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (Entity.Context.ContextMode == ContextModesTypes.ServerMode)
            {
                // Todo implement roll back in memory repos
                var repos = new Dictionary<string, IPersistanceRepository>();
                var changes = new List<EntityChanges>();
                var output = new QueryResult();

                var addAndReturnRepo = new Func<string, IPersistanceRepository, IPersistanceRepository>((type, repo) =>
                {
                    repos.Add(type, repo);
                    return repo;
                });

                foreach (var action in UowActions)
                {
                    var type = action.EntityChange.EntityModelType;

                    var repo = repos.ContainsKey(type) ?
                        repos[type] :
                        addAndReturnRepo(type, DepCon.ResolveRepository(type) as IPersistanceRepository);

                    switch (action.ActionInfoType)
                    {
                        case ActionInfoTypes.Save:

                            if (action.EntityChange.IsNew || action.EntityChange.MainEntityId == default(Guid) || await repo.FindEntityAsync(action.EntityChange.MainEntityId) == null)
                            {
                                var entity = MetadataHandler.ModelsByTypeName[type].Constructor();
                                entity.ApplyChanges(action.EntityChange.Changes);
                                if (entity.Id == default(Guid))
                                    entity.Id = action.EntityChange.MainEntityId != default(Guid) ? action.EntityChange.MainEntityId : Guid.NewGuid();

                                //entity.ValidationAction().Then = (qr) =>
                                //{
                                //    if (qr.IsSuccess)
                                //    {                                   
                                await repo.AddEntityAsync(entity);
                                //    }
                                //    else
                                //    {
                                //        output = new QueryResult() { IsSuccess = false, Message = "error in entity save validation" };
                                //    }
                                //};
                            }
                            else
                            {
                                var entity = ((await repo.FindEntityAsync(action.EntityChange.MainEntityId)).ObjectValue as Entity).CloneAction();
                                entity.ApplyChanges(action.EntityChange.Changes);

                                //entity.ValidationAction().Then = (qr) =>
                                //{
                                //    if (qr.IsSuccess)
                                //    {
                                await repo.UpdateEntityAsync(entity);
                                //    }
                                //    else
                                //    {
                                //        output = new QueryResult() { IsSuccess = false, Message = "error in entity save validation" };
                                //    }
                                //};
                            }

                            break;
                        default:
                        case ActionInfoTypes.Delete:
                            //Todo
                            throw new NotImplementedException();
                    }
                }

                // if no errors
                return new QueryResult() { IsSuccess = true, ObjectValue = true };
            }
            else
            {
                return await SendToServerAsync(actionInfo, trace);
            }
        }

        async Task<QueryResult> SendToServerAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {            
            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.HandleEntity,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                UnitOfWorkInfo = new Services.Operations.UnitOfWorkInfo()
            };

            paramsCarrier.UnitOfWorkInfo.SetChanges(this);

            var svc = new ServiceInvoker();
            var svcr = await svc.Send(paramsCarrier);

            return svcr;                
        }
        

        public void Dispose()
        {
        }
    }
}

﻿using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public class SynchronizedCache
    {
        private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();
        private Dictionary<Guid, Entity> innerCache = new Dictionary<Guid, Entity>();

        public int Count
        { get { return innerCache.Count; } }

        public Entity Read(Guid key)
        {
            cacheLock.EnterReadLock();
            try
            {
                return innerCache[key];
            }
            finally
            {
                cacheLock.ExitReadLock();
            }
        }

        public IEnumerable<Entity> ReadAll()
        {
            cacheLock.EnterReadLock();
            try
            {
                return innerCache.Values.ToList();
            }
            finally
            {
                cacheLock.ExitReadLock();
            }
        }

        public List<T> ReadAll<T>() where T : Entity
        {
            cacheLock.EnterReadLock();
            try
            {
                return innerCache.Values.OfType<T>().ToList();
            }
            finally
            {
                cacheLock.ExitReadLock();
            }
        }

        public void Add(Guid key, Entity value)
        {
            cacheLock.EnterWriteLock();
            try
            {
                if(!innerCache.ContainsKey(key))
                    innerCache.Add(key, value);
            }
            finally
            {
                cacheLock.ExitWriteLock();
            }
        }

        public bool AddWithTimeout(Guid key, Entity value, int timeout)
        {
            if (cacheLock.TryEnterWriteLock(timeout))
            {
                try
                {
                    innerCache.Add(key, value);
                }
                finally
                {
                    cacheLock.ExitWriteLock();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public AddOrUpdateStatus AddOrUpdate(Guid key, Entity value)
        {
            cacheLock.EnterUpgradeableReadLock();
            try
            {
                Entity result = null;
                if (innerCache.TryGetValue(key, out result))
                {
                    if (result == value)
                    {
                        return AddOrUpdateStatus.Unchanged;
                    }
                    else
                    {
                        cacheLock.EnterWriteLock();
                        try
                        {
                            value.ParentEntity = result;
                            innerCache[key].ApplyChanges(value.GetChanges().Changes);
                        }
                        finally
                        {
                            cacheLock.ExitWriteLock();
                        }
                        return AddOrUpdateStatus.Updated;
                    }
                }
                else
                {
                    cacheLock.EnterWriteLock();
                    try
                    {
                        innerCache.Add(key, value);
                    }
                    finally
                    {
                        cacheLock.ExitWriteLock();
                    }
                    return AddOrUpdateStatus.Added;
                }
            }
            finally
            {
                cacheLock.ExitUpgradeableReadLock();
            }
        }

        public void Delete(Guid key)
        {
            cacheLock.EnterWriteLock();
            try
            {
                innerCache.Remove(key);
            }
            finally
            {
                cacheLock.ExitWriteLock();
            }
        }

        public enum AddOrUpdateStatus
        {
            Added,
            Updated,
            Unchanged
        };

#if !NETJS
        ~SynchronizedCache()
        {
            if (cacheLock != null) cacheLock.Dispose();
        }
#endif
    }

    public class MemDbSet
    {
        static SynchronizedCache _cache;
        public static SynchronizedCache Cache
        {
            get
            {
                return _cache ?? (_cache = new SynchronizedCache());
            }
        }

        public static object AddLocker = new object();
    }

    public class MemDbSet<T> : MemDbSet, IDbSet<T> where T : Entity
    {
        List<T> CacheItems
        {
            get
            {
                return Cache.ReadAll<T>();
            }
        }

        public IDependencyContainer DepCon { get; set; }

        public int Count
        {
            get
            {
                return Cache.Count;
            }
        }

        public T Add(T entity)
        {
            lock (AddLocker)
            {
                if (entity.Id == default(Guid))
                    entity.Id = Guid.NewGuid();

                Cache.Add(entity.Id, entity);
                return entity;                
            }
        }

        public async Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = Add(entity);
                return new QueryResult<T>() { Value = result, IsSuccess = result == null };
            });
        }

        public T Update(T entity)
        {
            if (entity.Id == default(Guid))
            {
                return null;
            }

            Cache.AddOrUpdate(entity.Id, entity);
            if (Cache.Read(entity.Id) == null)
            {
                return null;
            }
            else
            {
                return entity;
            }
        }

        public async Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = Update(entity);
                return new QueryResult<T>() { Value = result, IsSuccess = result == null };
            });
        }

        public bool Delete(T entity)
        {
            return Delete(entity.Id);
        }

        public bool Delete(Guid id)
        {
            if (Cache.Read(id) != null)
            {
                Cache.Delete(id);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<QueryResult<Entity>> AddObjectAsync(Entity entity)
        {
            return (await AddAsync(entity as T)).CastTo<Entity>();
        }

        public async Task<QueryResult<Entity>> FindObjectAsync(Guid id)
        {
            return (await FindAsync(id)).CastTo<Entity>();
        }

        public async Task<QueryResult<Entity>> UpdateObjectAsync(Entity entity)
        {
            return (await UpdateAsync(entity as T)).CastTo<Entity>();
        }

        #region Enumerable

        public IEnumerator<T> GetEnumerator()
        {
            return CacheItems.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return CacheItems.GetEnumerator();
        }

        public Task<List<T>> ToListAsync()
        {
            return Task.Run(() =>
            {                
                return CacheItems;
            });
        }

        public IQueryable<T> ToQueryableAsync()
        {
            return CacheItems.AsQueryable();
        }

        #endregion

        public async Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = Delete(entity);
                return new QueryResult() { IsSuccess = result };
            });
        }

        public async Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = Delete(id);
                return new QueryResult() { IsSuccess = result };
            });
        }

        public async Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = this.FirstOrDefault(condition.Build<T, bool>());
                return new QueryResult<T>() { IsSuccess = true, Value = result };
            });
        }               

        public async Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = this.Any(condition.Build<T, bool>());
                return new QueryResult() { IsSuccess = result };
            });
        }

        public async Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = this.Where(condition.Build<T, bool>()).ToList();
                return new QueryResult<List<T>>() { IsSuccess = true, Value = result };
            });
        }

        public async Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = this.ToList();
                return new QueryResult<List<T>>() { IsSuccess = true, Value = result };
            });
        }


        public T Find(Guid id)
        {
            if (Cache.Read(id) != null)
                return Cache.Read(id) as T;

            return null;
        }

        public async Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = MemDbSet.Cache.Read(id) as T;
                return new QueryResult<T>() { IsSuccess = true, Value = result };
            });
        }

        public void Dispose()
        {
            
        }

        public async Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var result = this.Count(condition.Build<T, bool>());
                return new QueryResult<int>() { IsSuccess = true, Value = result };
            });
        }
    }
}

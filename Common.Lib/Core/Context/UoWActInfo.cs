﻿using System;
using Common.Lib.Core.Tracking;

namespace Common.Lib.Core.Context
{
    public enum ActionInfoTypes
    {
        Save,
        Delete
    }

    public struct UoWActInfo
    {
        public EntityChanges EntityChange { get; set; }
        public string UserId { get; set; }
        public string DeviceId { get; set; }

        public DateTime ActionTime { get; set; }

        public ActionInfoTypes ActionInfoType { get; set; }
    }
}

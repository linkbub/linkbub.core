﻿using System;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Core.Context
{
    public class ClientInfo
    {
        public Guid LogonUserId { get; set; }

        public string LogonToken { get; set; }

        public string DeviceId { get; set; }

        public ClientDeviceTypes DeviceType { get; set; }

        Action<ClientInfo> StoreAction { get; set; }

        public void SetStoreAction(Action<ClientInfo> storeAction)
        {
            StoreAction = storeAction;
        }

        public void Store()
        {
            if (StoreAction != null)
                StoreAction(this);
        }
    }
}

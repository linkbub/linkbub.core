﻿using System;
using System.Linq.Expressions;

namespace Common.Lib.Core
{
    public interface IExpressionBuilder
    {
        Expression<Func<TEntity, bool>> CreateExpression<TEntity>() where TEntity : Entity;
        Expression<Func<T, TKey>> CreateExpression<T, TKey>() where T : Entity;

        Func<TEntity, bool> Build<TEntity>() where TEntity : Entity;
        Func<T, TKey> Build<T, TKey>() where T : Entity;

        object[] GetParameters();

        void SetParameters(object[] parameters);
    }
}

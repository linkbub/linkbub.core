﻿namespace Common.Lib.Core
{
    public enum ImageDataSizeTypes
    {
        Thumb,
        Medium,
        Original
    }
}

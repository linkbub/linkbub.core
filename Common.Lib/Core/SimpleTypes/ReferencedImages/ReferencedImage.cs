﻿using System;

namespace Common.Lib.Core
{
    public class ReferencedImage : SimpleType
    {
        public ReferencedImage(Func<string> idGetter, Action<string> idSetter) 
            : base(idGetter, idSetter)
        {
        }
    }
}

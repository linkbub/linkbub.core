﻿using System;
using Common.Lib.Serialization;

namespace Common.Lib.Core
{
    public class ReferencedImageData
    {
        public Guid EntityId { get; set; }

        public string ImageId { get; set; }

        public byte[] Image { get; set; }

        public bool IsSync { get; set; }

        public ImageDataSizeTypes CurrentSize { get; set; }

        public override string ToString()
        {
            var output = "{I¬" + ImageId;

            if (EntityId != default(Guid))
                output += "|E¬" + EntityId.ToString();

            if (Image != null && Image.Length > 0)
                output += "|B¬" + CommonSerializer.EncodeTo64(Image);

            output += "|S¬" + (int)CurrentSize + "}";

            return CommonSerializer.EncodeTo64(output);
        }

        public static ReferencedImageData FromString(string chain)
        {
            var output = new ReferencedImageData();
            output.FromStringInternal(CommonSerializer.DecodeFrom64(chain));
            return output;
        }

        public void FromStringInternal(string chain)
        {
            CommonSerializer.ProcesPairItems(chain, (item) =>
            {
                switch (item.Key)
                {
                    case "E":
                        EntityId = new Guid(item.Value.ToString());
                        break;

                    case "I":
                        ImageId = item.Value.ToString();
                        break;

                    case "B":
                        Image = CommonSerializer.DecodeFrom64ToByteArray(item.Value.ToString());
                        break;

                    case "S":
                        CurrentSize = (ImageDataSizeTypes)int.Parse(item.Value.ToString());
                        break;
                }
            });
        }
    }

}

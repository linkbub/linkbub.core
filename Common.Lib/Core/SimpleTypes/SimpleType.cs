﻿using System;

namespace Common.Lib.Core
{
    public class SimpleType
    {
        public string Id
        {
            get
            {
                return IdGetter();
            }
            set
            {
                IdSetter(value);
            }
        }

        public Func<string> IdGetter;
        public Action<string> IdSetter;

        public SimpleType(Func<string> idGetter, Action<string> idSetter)
        {
            IdGetter = idGetter;
            IdSetter = idSetter;
        }

        public override string ToString()
        {
            return Id;
        }
    }
}

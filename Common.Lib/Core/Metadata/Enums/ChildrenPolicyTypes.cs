﻿namespace Common.Lib.Core.Metadata
{
    public enum ChildrenPolicyTypes
    {
        None = 0,
        FirstLevel = 1,
        SecondLevel = 2,
        ThirdLevel = 3
    }
}

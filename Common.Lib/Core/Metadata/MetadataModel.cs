﻿using System;
using System.Collections;

namespace Common.Lib.Core
{
    public class MetadataModel
    {
        public Type ModelType { get; set; }
        public string ModelTypeFullName { get; set; }
        public Type ListType { get; set; }
        public Type ParentModelType { get; set; }
        public Func<Entity> Constructor { get; set; }
        public Func<IList> ListConstructor { get; set; }

        public MetadataModel()
        {
        }

        public bool IsChildOf(Type type)
        {
            if (ParentModelType == type)
                return true;
            else if (MetadataHandler.Models.ContainsKey(ParentModelType) && MetadataHandler.Models[ParentModelType].IsChildOf(type))
                return true;
            else
                return false;
        }

        public bool IsCollectionTypeOf(Type type)
        {
            if (ListType == type)
                return true;
            else if (ParentModelType == null || !MetadataHandler.Models.ContainsKey(ParentModelType))
                return false;
            else
                return MetadataHandler.Models[ParentModelType].IsCollectionTypeOf(type);
        }
    }
}

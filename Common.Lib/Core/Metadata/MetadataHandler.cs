﻿using Common.Lib.Authentication;
using Common.Lib.Babylon;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.Lib.Core
{
    public static class MetadataHandler
    {
        public static Dictionary<Type, MetadataModel> ModelsByListType = new Dictionary<Type, MetadataModel>();

        public static Dictionary<Type, MetadataModel> Models = new Dictionary<Type, MetadataModel>();

        public static Dictionary<string, MetadataModel> ModelsByTypeName = new Dictionary<string, MetadataModel>();

        public static void RegisterCoreModels()
        {
            MetadataHandler.AddMetadataModel<User>(typeof(List<User>), typeof(Entity), () => { return new User(); }, () => { return new List<User>(); });
            MetadataHandler.AddMetadataModel<BabylonConcept>(typeof(List<BabylonConcept>), typeof(Entity), () => { return new BabylonConcept(); }, () => { return new List<BabylonConcept>(); });
        }

        public static void AddMetadataModel<T>(Type listType,
                                                Type parentModelType,
                                                Func<Entity> constructor,
                                                Func<IList> listConstructor) where T : Entity
        {
            var type = typeof(T);

            if (!Models.ContainsKey(type))
            {
                var modelMetadata = new MetadataModel()
                {
                    ModelType = type,
                    ModelTypeFullName = type.FullName,
                    ListType = listType,
                    ParentModelType = parentModelType,
                    Constructor = constructor,
                    ListConstructor = listConstructor
                };

                Models.Add(type, modelMetadata);
                ModelsByTypeName.Add(type.FullName, modelMetadata);
                ModelsByListType.Add(listType, modelMetadata);
            }
        }
    }
}

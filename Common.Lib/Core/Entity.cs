﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Common.Lib.Babylon;
using Common.Lib.Core.Context;
using Common.Lib.Core.Metadata;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Serialization;
using Common.Lib.Services;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Core
{
    public class Entity
    {
        #region static

        public static IClientRepository<Entity> ClientRepository
        {
            get
            {
                return (IClientRepository<Entity>)Repository<Entity>();
            }
        }

        public static IRepository<T> Repository<T>() where T : Entity, new()
        {
            return Context.DepCon.ResolveRepository<T>();
        }

        public static T New<T>() where T : Entity, new()
        {
            return new T()
            {
                Id = Guid.NewGuid(),
                IsNew = true
            };
        }

        public static IContext Context { get; set; }

        public static T DeserializeComprised<T>(string input) where T : Entity, new()
        {
            return Deserialize<T>(CommonSerializer.DecodeFrom64(input));
        }

        public static T Deserialize<T>(string input) where T : Entity, new()
        {
            using (var serializer = Context.DepCon.ResolveAndConstruct<IQueryResultSerializer>())
            {
                var qr = serializer.DeserializeQueryResult<T>(input);
                var output = qr.Value;

                // todo: reimplement when neeeded
                //if (Context.ContextMode == ContextModesTypes.ClientMode)
                //{
                //    using (var repo = Context.DepCon.ResolveRepository<T>())
                //    {
                //        repo.AddAndCallBindings(new List<Entity>() { output });
                //    }
                //}

                return output;
            }
        }

        public static IUnitOfWork UnitOfWork
        {
            get
            {
                return Context.DepCon.ResolveAndConstruct<IUnitOfWork>();
            }
        }

        #endregion

        #region Stored Properties

        [Metadata(EntityMetadata.Id)]
        public Guid Id { get; set; }

        [Metadata(EntityMetadata.CreatedOn)]
        public DateTime CreatedOn { get; set; }

        [Metadata(EntityMetadata.UpdatedOn)]
        public DateTime UpdatedOn { get; set; }

        #endregion

        #region Not Stored Properties

        /// <summary>
        /// Used when cloning an entity to compare
        /// </summary>
        [JsonIgnore]
        public object ParentEntity;

        public bool IsNew { get; set; }

        [JsonIgnore]
        public string TypeName;

        [JsonIgnore]
        public Func<ActionInfo, OperationInfo, Task<QueryResult>> SaveAction;

        [JsonIgnore]
        public Func<ActionInfo, OperationInfo, Task<QueryResult>> DeleteAction;

        [JsonIgnore]
        public Func<Entity> CloneAction;

        //[JsonIgnore]
        //public Func<Promise<QueryResult>> ValidationAction { get; set; }


        public ValidationResult CurrentValidation { get; private set; }

        #endregion

        public Entity()
        {
            TypeName = this.GetType().FullName;
        }

        #region Save
        public virtual void Save(IUnitOfWork uow)
        {
            uow.AddEntitySave(this);
        }

        #region verify deletion
        //public virtual async Task Persist<T>(QueryResult<T> qr = null) where T : Entity, new()
        //{

        //    var output = qr == null ? new QueryResult() : qr;

        //    CurrentValidation = Validate();

        //    if (CurrentValidation.IsSuccess)
        //    {
        //        using (var repo = Context.DepCon.ResolveRepository<T>())
        //        {
        //            if (this.Id == default(Guid))
        //            {
        //                this.CreatedOn = DateTime.Now;
        //                this.UpdatedOn = DateTime.Now;

        //                var resultAdd = repo.Add(this as T);

        //                output.IsSuccess = resultAdd != null;
        //                output.ObjectValue = resultAdd;
        //                output.ResultNum = output.IsSuccess ?
        //                    (int)SaveEntityResultsTypes.Ok :
        //                    (int)SaveEntityResultsTypes.DbAddError;
        //            }
        //            else
        //            {
        //                if (repo.All(x => x.Id == this.Id))
        //                {
        //                    output.IsSuccess = false;
        //                    output.ResultNum = (int)SaveEntityResultsTypes.CannotUpdateAsTheIdWasNotFound;
        //                }
        //                else
        //                {
        //                    this.UpdatedOn = DateTime.Now;
        //                    var resultUpdate = repo.Update(this as T);

        //                    output.IsSuccess = resultUpdate != null;
        //                    output.ObjectValue = resultUpdate;
        //                    output.ResultNum = output.IsSuccess ?
        //                        (int)SaveEntityResultsTypes.Ok :
        //                        (int)SaveEntityResultsTypes.DbAddError;
        //                }
        //            }
        //        }
        //    }
        //    return output;


        //    return SaveAsync<T>(qr).Result;
        //}
        #endregion

        public virtual async Task<QueryResult<T>> SaveAsync<T>(QueryResult<T> qr = null, ActionInfo actionInfo = null, OperationInfo trace = null) where T : Entity, new()
        {
            qr = qr == null ? new QueryResult<T>() : qr;

            CurrentValidation = Validate();

            if (CurrentValidation.IsSuccess)
            {
                using (var repo = Context.DepCon.ResolveRepository<T>())
                {
                    if (this.Id == default(Guid) || IsNew)
                    {
                        //todo: move to local repository responsability
                        //this.CreatedOn = DateTime.Now;
                        //this.UpdatedOn = DateTime.Now;

                        var resultAdd = await repo.AddAsync(this as T);
                        if (resultAdd.IsSuccess)
                        {
                            qr.IsSuccess = true;
                            qr.ResultNum = (int)SaveEntityResultsTypes.Ok;
                            qr.Value = resultAdd.Value;
                        }
                        else
                        {
                            qr.IsSuccess = false;
                            qr.ResultNum = resultAdd.ResultNum;
                            qr.Value = null;
                        }

                        return qr;

                    }
                    else
                    {
                        var resultUpdate = await repo.UpdateAsync(this as T);

                        if (resultUpdate.IsSuccess)
                        {
                            qr.IsSuccess = true;
                            qr.ResultNum = 1;
                            qr.Value = resultUpdate.Value;
                        }
                        else
                        {
                            qr.IsSuccess = false;
                            qr.ResultNum = resultUpdate.ResultNum;
                        }

                        return qr;
                    }
                }
            }
            else
            {
                qr.Errors = CurrentValidation.Errors;
                return qr;
            }
        }

        public virtual ValidationResult Validate()
        {
            var output = new ValidationResult()
            {
                IsSuccess = true
            };

            return output;
        }

        #endregion

        #region Delete
        public virtual void Delete(IUnitOfWork uow)
        {
            //Todo UOW Delete
            //uow.(this);
            throw new NotImplementedException();
        }

        #region review delete
        //public virtual QueryResult Delete<T>(QueryResult<T> qr = null) where T : Entity, new()
        //{
        //    var output = qr == null ? new QueryResult() : qr;

        //    using (var repo = Context.DepCon.ResolveRepository<T>())
        //    {
        //        if (this.Id == default(Guid))
        //        {
        //            output.IsSuccess = false;
        //            output.ResultNum = (int)SaveEntityResultsTypes.LogicError;
        //        }
        //        else
        //        {
        //            var resultDelete = repo.Delete(this as T);

        //            output.IsSuccess = resultDelete;
        //            output.ObjectValue = resultDelete;
        //            output.ResultNum = output.IsSuccess ?
        //                (int)SaveEntityResultsTypes.Ok :
        //                (int)SaveEntityResultsTypes.DbDeleteError;
        //        }
        //    }

        //    return output;
        //}

        #endregion

        public virtual async Task<QueryResult> DeleteAsync<T>(QueryResult qr = null, ActionInfo actionInfo = null, OperationInfo trace = null) where T : Entity, new()
        {
            qr = qr == null ? new QueryResult() : qr;

            if (this.Id == default(Guid))
            {
                qr.IsSuccess = false;
                qr.ResultNum = (int)SaveEntityResultsTypes.CannotDeleteAnEmptyId;
                return qr;
            }

            using (var repo = Context.DepCon.ResolveRepository<T>())
            {
                var resultDelete = await repo.DeleteAsync(this as T);
                
                if (resultDelete.IsSuccess)
                {
                    qr.IsSuccess = true;
                    qr.ResultNum = (int)SaveEntityResultsTypes.Ok;
                }
                else
                {
                    qr.IsSuccess = false;
                    qr.ResultNum = resultDelete.ResultNum;
                }

                return qr;                                
            }
        }

        #endregion

        #region Get Changes and Clone

        public virtual T Clone<T>() where T : Entity, new()
        {
            var output = new T
            {
                Id = Id,
                CreatedOn = CreatedOn,
                UpdatedOn = UpdatedOn,

                ParentEntity = this
            };

            output.CreatePropertiesHandler();

            return output;
        }

        public virtual EntityChanges GetChanges()
        {
            var output = new EntityChanges
            {
                IsNew = IsNew,
                IsNewMainEntity = IsNew,
                MainEntityId = this.Id,
                EntityModelType = this.GetType().FullName,
            };

            if (this.ParentEntity == null)
            {
                this.CreatedOn = DateTime.Now;
                output.AddChange(EntityMetadata.CreatedOn, this.CreatedOn);
            }

            this.UpdatedOn = DateTime.Now;
            output.AddChange(EntityMetadata.UpdatedOn, this.UpdatedOn);

            return output;
        }

        public void ApplyChanges(List<ChangeUnit> changes)
        {
            var propertiesHandler = CreatePropertiesHandler();
            foreach (var change in changes)
            {
                propertiesHandler.Setters[change.MetadataId](change.Value);
            }
        }

        public virtual PropertiesHandler CreatePropertiesHandler()
        {
            var output = new PropertiesHandler();

            output.RegisterSetter(EntityMetadata.CreatedOn, (o) => { CreatedOn = ConvertToDateTime(o); });
            output.RegisterSetter(EntityMetadata.UpdatedOn, (o) => { UpdatedOn = ConvertToDateTime(o); });

            return output;
        }

        public virtual Dictionary<int, string> ObtainTranslations(int language)
        {
            return new Dictionary<int, string>();
        }

        public virtual void AssignTranslations(IEnumerable<EntityTranslation> translations)
        {

        }

        public async virtual Task IncludeChildren(int pendingLevels, bool includeBabylonConcepts, QueryResult qr)
        {

        }

        public virtual void ExcludeChildren()
        {

        }
        public async virtual Task IncludeBabylonConcepts()
        {

        }

        public virtual void ExcludeBabylonConcepts()
        {

        }

        #endregion

        #region Converters

        public DateTime ConvertToDateTime(object o)
        {
            if (o is DateTime)
                return (DateTime)o;
            else if(o is int)
            {
                return new DateTime().AddTicks((int)o);
            }
            else 
            {
                if (long.TryParse(o.ToString(), out long ticks))
                {
                    return new DateTime().AddTicks(ticks);
                }
                else if (int.TryParse(o.ToString(), out int ticks2))
                {
                    return new DateTime().AddTicks(ticks2);
                }
            }           
            
            return DateTime.Parse(o.ToString());            
        }
        
        public Guid ConvertToGuid(object o)
        {

#if !UNITY
            if (o is System.Text.Json.JsonElement)
            {
                var s = ((System.Text.Json.JsonElement)o).GetString();
                return new Guid(s);
            }
#endif

            if (o == null)
            {
                return default(Guid);
            }
            else if (o is string && !string.IsNullOrEmpty((string)o))
            {
                return new Guid((string)o);
            }
            else if (o is Guid)
            {
                return (Guid)o;
            }
            else
            {
                return default(Guid);
            }
        }

        public List<Guid> ConvertToGuids(object o)
        {
            if (o == null)
            {
                return default(List<Guid>);
            }
            else if (o is string && !string.IsNullOrEmpty((string)o))
            {
                return CommonSerializer.DeserializeAction<List<Guid>>(o.ToString());
            }
            else if (o is List<Guid>)
            {
                return (List<Guid>)o;
            }
            else
            {
                return default(List<Guid>);
            }
        }

        public Guid ConvertToGuid(string o)
        {
            if (string.IsNullOrEmpty(o))
            {
                return default(Guid);
            }
            else
            {
                return new Guid(o);
            }
        }

        public string ConvertToString(object o)
        {
            return o == null ? string.Empty : o.ToString();
        }

        public int ConvertToInt(object o)
        {

#if !UNITY
            if (o is System.Text.Json.JsonElement)
            {
                var i = ((System.Text.Json.JsonElement)o).GetInt32();
                return i;
            }
#endif
            if (o is long)
                return (int)(long)o;
            else if (o is string)
                return int.Parse((string)o);
            else
                return (int)o;
        }

        public int ConvertToInt(int i)
        {
            return i;
        }

        public int ConvertToInt(string s)
        {
            int.TryParse(s, out int output);
            return output;
        }

        public bool ConvertToBool(object o)
        {
            if (o is string)
                return (string)o == "true";
            else if (o is int)
                return ((int)o) != 0;
            else
                return bool.Parse(o.ToString());
        }

        public double ConvertToDouble(object o)
        {
            if (o is string)
                return CommonSerializer.ParseDouble((string)o);
            else if (o is double || o is float || o is int)
                return (double)o;
            else
                return CommonSerializer.ParseDouble(o.ToString());
        }

        public string ConvertComplexType(object o)
        {
            return CommonSerializer.DecodeFrom64(o.ToString());
        }
            

        public CT CreateComplexType<CT>(string serialzedComplexType, Action<string> setter) where CT : ComplexType, new()
        {
            CT output = default(CT);

            if (string.IsNullOrEmpty(serialzedComplexType) || serialzedComplexType == "[]")
            {
                output = new CT();
            }
            else
            {
                output = CommonSerializer.DeserializeEncoded<CT>(serialzedComplexType);
            }
            output.Setter = setter;
            return output;
        }

        public CTL CreateComplexTypeList<CTL, CT>(string serialzedComplexTypeList, Action<string> setter) where CTL : ComplexTypeList<CT>, new() where CT : ComplexType
        {
            CTL output = new CTL();

            if (string.IsNullOrEmpty(serialzedComplexTypeList) || serialzedComplexTypeList == "[]")
            {
                //output = new CTL();
            }
            else
            {
                var regex = new Regex("},{");
                var pre = serialzedComplexTypeList.Substring(1, serialzedComplexTypeList.Length - 2);

                var spaso = regex.Split(pre);

                if (spaso.Length > 0)
                {
                    if (spaso.Length == 1)
                    {
                        output.Add(CommonSerializer.DeserializeAction<CT>(spaso[0]));
                    }
                    else if (spaso.Length == 2)
                    {
                        var os1 = spaso[0] + "}";
                        output.Add(CommonSerializer.DeserializeAction<CT>(os1));

                        var os2 = "{" + spaso[1];
                        output.Add(CommonSerializer.DeserializeAction<CT>(os2));
                    }
                    else
                    {
                        var osP = spaso[0] + "}";
                        output.Add(CommonSerializer.DeserializeAction<CT>(osP));

                        for (var i = 1; i < spaso.Length - 2; i++)
                        {
                            var os = "{" + spaso[i] + "}";
                            output.Add(CommonSerializer.DeserializeAction<CT>(os));
                        }

                        var osL = "{" + spaso[1];
                        output.Add(CommonSerializer.DeserializeAction<CT>(osL));
                    }
                }
                else
                {
                    
                }

                //output = (CTL)CommonSerializer.DeserializeAsObjectAction(serialzedComplexTypeList, typeof(CTL));
            }

            output.Setter = setter;
            return output;
        }

        public STL CreateSimpleTypeList<STL, ST>(string serialzedComplexTypeList, Action<string> setter) where STL : SimpleTypeList<ST>, new()
        {
            STL output = default(STL);

            if (string.IsNullOrEmpty(serialzedComplexTypeList) || serialzedComplexTypeList == "[]")
            {
                output = new STL();
            }
            else
            {
                output = CommonSerializer.DeserializeEncoded<STL>(serialzedComplexTypeList);
            }
            output.Setter = setter;
            return output;
        }

        #endregion

        #region Serialization

        public string SelfSerializeAndCompress<T>() where T : Entity
        {
            return CommonSerializer.EncodeTo64(SelfSerialize<T>());
        }

        public string SelfSerialize<T>() where T : Entity
        {
            using (var serializer = Context.DepCon.ResolveAndConstruct<IQueryResultSerializer>())
            {
                var queryResult = new QueryResult<T>
                {
                    IsSuccess = true,
                    Value = this as T
                };

                return queryResult.Serialize(Context.DepCon);
            }
        }

        public virtual void Serialize(IQueryResultSerializer serializer, bool includeChildren)
        {
            serializer.AddEntityProperty(this.TypeName, this.Id, EntityMetadata.Id, Id);
            serializer.AddEntityProperty(this.TypeName, this.Id, EntityMetadata.CreatedOn, CreatedOn);
            serializer.AddEntityProperty(this.TypeName, this.Id, EntityMetadata.UpdatedOn, UpdatedOn);
        }

        public virtual void Deserialize(IQueryResultSerializer deserializer, Dictionary<int, string> data)
        {
            Id = new Guid(data[EntityMetadata.Id]);

            if (data.ContainsKey(EntityMetadata.CreatedOn))
                CreatedOn = new DateTime().AddTicks(long.Parse(data[EntityMetadata.CreatedOn]));

            if (data.ContainsKey(EntityMetadata.UpdatedOn))
                CreatedOn = new DateTime().AddTicks(long.Parse(data[EntityMetadata.UpdatedOn]));
        }



        #endregion
    }

    public static class EntityMetadata
    {
        public const int Id = 1;
        public const int CreatedOn = 2;
        public const int UpdatedOn = 3;
        public const int Last = UpdatedOn;
    }

    public enum SaveEntityResultsTypes
    {
        Ok = 1,
        DbAddError = 2,
        DbUpdateError = 3,
        DbDeleteError = 4,
        CannotUpdateAsTheIdWasNotFound = 5,
        LogicError = 6,
        CannotDeleteAnEmptyId = 8
    }
}

﻿using Common.Lib.Serialization;
using System;

#if UNITY
using Newtonsoft.Json;
#else
using System.Text.Json.Serialization;
#endif

namespace Common.Lib.Core
{
    public class ComplexType
    {
        [JsonIgnore]
        public Action<string> Setter;

        public void Store()
        {
            Setter(CommonSerializer.SerializeAction(this));
        }

        public ComplexType()
        {

        }
    }
}

﻿using Common.Lib.Serialization;
using System;
using System.Collections.Generic;

namespace Common.Lib.Core.Tracking
{
    public class EntityChanges : Entity
    {
        #region Stored Properties

        public string EntityModelType { get; set; }

        public Guid MainEntityId { get; set; }

        public bool IsNewMainEntity { get; set; }

        public bool IsRemoved { get; set; }

        public List<ChangeUnit> Changes { get; set; }

        #endregion

        public EntityChanges()
        {
            Changes = new List<ChangeUnit>();
        }
        public void AddChange(int metadataId, object value)
        {
            if (value is ComplexType)
                Changes.Add(new ChangeUnit() { MetadataId = metadataId, Value = CommonSerializer.SerializeAndEncode(value) });
            else
                Changes.Add(new ChangeUnit() { MetadataId = metadataId, Value = value });
        }

        public void Serialize(IChangesSerializer serializer)
        {
            serializer.AddEntityChanges(this, EntityModelType);
        }
    }

    public class ChangeUnit
    {
        public int MetadataId { get; set; }

        public object Value { get; set; }
    }
}

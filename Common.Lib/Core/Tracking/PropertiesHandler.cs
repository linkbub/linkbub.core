﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Lib.Core.Tracking
{
    public class PropertiesHandler
    {
        public Dictionary<int, Action<object>> Setters { get; set; }

        public PropertiesHandler()
        {
            Setters = new Dictionary<int, Action<object>>();
        }

        public void RegisterSetter(int metadataId, Action<object> setter)
        {
            if (Setters.ContainsKey(metadataId))
                throw new Exception($"Metadata {metadataId} repeated");

            Setters.Add(metadataId, setter);

        }
    }
}

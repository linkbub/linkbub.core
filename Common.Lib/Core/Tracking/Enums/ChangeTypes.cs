﻿namespace Common.Lib.Core.Tracking
{
    public enum ChangeTypes
    {
        Add,
        AddRange,
        Update,
        UpdateRange,
        Delete,
        DeleteRange
    }
}

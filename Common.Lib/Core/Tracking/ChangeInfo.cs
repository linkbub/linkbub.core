﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Lib.Core.Tracking
{
    public class ChangeInfo
    {
        public Guid Id { get; set; }
        public Guid MainEntityId { get; set; }

        public string TypeName { get; set; }

        public ChangeTypes ChangeType { get; set; }

        public string RangeCode { get; set; }

        public DateTime Time { get; set; }

        public List<ChangeUnit> Changes { get; set; }

        public ChangeInfo()
        {
            Changes = new List<ChangeUnit>();
        }
    }
}

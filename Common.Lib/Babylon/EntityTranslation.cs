﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Lib.Babylon
{
    public class EntityTranslation
    {
        public Guid EntityId { get; set; }

        public int PropertyId { get; set; }

        public string Text { get; set; }
    }
}

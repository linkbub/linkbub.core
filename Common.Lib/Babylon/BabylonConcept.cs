﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Metadata;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Serialization;
using Common.Lib.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common.Lib.Babylon
{
    public class BabylonConcept : Entity
    {
        #region statics

        public static IClientRepository<BabylonConcept> ClientRepository
        {
            get
            {
                return (IClientRepository<BabylonConcept>)Repository;
            }
        }

        public static IRepository<BabylonConcept> Repository
        {
            get
            {
                return Repository<BabylonConcept>();
            }
        }
        public static BabylonConcept New()
        {
            return New<BabylonConcept>();
        }

        public static BabylonConcept NewFromData(string es, string en, string cat)
        {
            var output = New();

            output.Code = "";
            output.Spanish = es;
            output.English = en;
            output.Catalan = cat;

            return output;
        }

        public static async Task<QueryResult<BabylonConcept>> FindAsync(Guid id)
        {
            using (var repo = Repository)
            {
                return await repo.FindAsync(id);
            }
        }

        #endregion

        #region Code

        [Metadata(BabylonConceptMetadata.Code)]
        public string Code { get; set; }

        #endregion

        #region Spanish


        [Metadata(BabylonConceptMetadata.Spanish)]
        public string Spanish { get; set; }

        #endregion

        #region English


        [Metadata(BabylonConceptMetadata.English)]
        public string English { get; set; }

        #endregion

        #region Catalan


        [Metadata(BabylonConceptMetadata.Catalan)]
        public string Catalan { get; set; }

        #endregion

        public BabylonConcept()
        {
            SaveAction = async (actionInfo, trace) => (await SaveAsync(actionInfo, trace)).CastTo<Entity>();
            DeleteAction = async (actionInfo, trace) => (await DeleteAsync(actionInfo, trace));

            CloneAction = Clone;
            //ValidationAction = SaveLogic;
        }


        #region Clone

        public BabylonConcept CloneBabylonConcept()
        {
            return this.Clone();
        }

        public BabylonConcept Clone()
        {
            return Clone<BabylonConcept>();
        }

        public override T Clone<T>()
        {
            var output = base.Clone<T>() as BabylonConcept;

            output.Code = Code;
            output.Spanish = Spanish;
            output.English = English;
            output.Catalan = Catalan;

            return output as T;
        }

        #endregion

        #region Changes

        public override EntityChanges GetChanges()
        {
            var output = base.GetChanges();

            if (ParentEntity == null)
            {
                if (!string.IsNullOrEmpty(Code))
                    output.AddChange(BabylonConceptMetadata.Code, Code);

                if (!string.IsNullOrEmpty(Spanish))
                    output.AddChange(BabylonConceptMetadata.Spanish, Spanish);

                if (!string.IsNullOrEmpty(English))
                    output.AddChange(BabylonConceptMetadata.English, English);

                if (!string.IsNullOrEmpty(Catalan))
                    output.AddChange(BabylonConceptMetadata.Catalan, Catalan);
            }
            else
            {
                var previous = ParentEntity as BabylonConcept;

                if (previous.Code != Code)
                    output.AddChange(BabylonConceptMetadata.Code, Code);

                if (previous.Spanish != Spanish)
                    output.AddChange(BabylonConceptMetadata.Spanish, Spanish);

                if (previous.English != English)
                    output.AddChange(BabylonConceptMetadata.English, English);

                if (previous.Catalan != Catalan)
                    output.AddChange(BabylonConceptMetadata.Catalan, Catalan);
            }

            return output;
        }

        public override PropertiesHandler CreatePropertiesHandler()
        {
            var propHandler = base.CreatePropertiesHandler();

            propHandler.RegisterSetter(BabylonConceptMetadata.Code, (o) => { Code = ConvertToString(o); });
            propHandler.RegisterSetter(BabylonConceptMetadata.Spanish, (o) => { Spanish = ConvertToString(o); });
            propHandler.RegisterSetter(BabylonConceptMetadata.English, (o) => { English = ConvertToString(o); });
            propHandler.RegisterSetter(BabylonConceptMetadata.Catalan, (o) => { Catalan = ConvertToString(o); });

            return propHandler;
        }

        #endregion

        #region Save

        public void SaveUow(IUnitOfWork uow)
        {
            this.Save(uow);
        }

        public async Task<QueryResult<BabylonConcept>> SaveAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await this.SaveAsync<BabylonConcept>(null, actionInfo, trace);
        }

        public async override Task<QueryResult<T>> SaveAsync<T>(QueryResult<T> qr = null, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var slr = await SaveLogic();
            
            if (slr.IsSuccess)
            {
                return await base.SaveAsync<T>(qr, actionInfo, trace);
            }
            else
            {
                return new QueryResult<T>() { IsSuccess = false, ResultNum = (int)SaveEntityResultsTypes.LogicError };
            }
        }

        public async Task<QueryResult> SaveLogic()
        {
            return await Task.Run(() =>
            {
                if (Context.ContextMode == ContextModesTypes.ClientMode)
                {
                    return new QueryResult() { IsSuccess = true };
                }
                else
                {
                    return new QueryResult() { IsSuccess = true };
                }
            });
        }

        #endregion

        #region Delete

        public void DeleteUow(IUnitOfWork uow)
        {
            this.Delete(uow);
        }

        public async Task<QueryResult> DeleteAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await this.DeleteAsync<BabylonConcept>(null, actionInfo, trace);
        }

        public override async Task<QueryResult> DeleteAsync<T>(QueryResult qr = null, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var dlr = await DeleteLogic();

            if (dlr.IsSuccess)
            {
                return await base.DeleteAsync<T>(qr, actionInfo, trace);
            }
            else
            {
                return new QueryResult() { IsSuccess = false, ResultNum = (int)SaveEntityResultsTypes.LogicError };
            }
        }

        public async Task<QueryResult> DeleteLogic()
        {
            return await Task.Run(() =>
            {
                if (Context.ContextMode == ContextModesTypes.ClientMode)
                {
                    return new QueryResult() { IsSuccess = true };
                }
                else
                {
                    return new QueryResult() { IsSuccess = true };
                }
            });
        }

        #endregion

        #region Serialization

        public override void Serialize(IQueryResultSerializer serializer, bool includeChildren)
        {
            base.Serialize(serializer, includeChildren);

            serializer.AddEntityProperty(this.TypeName, this.Id, BabylonConceptMetadata.Code, Code);
            serializer.AddEntityProperty(this.TypeName, this.Id, BabylonConceptMetadata.Spanish, Spanish);
            serializer.AddEntityProperty(this.TypeName, this.Id, BabylonConceptMetadata.English, English);
            serializer.AddEntityProperty(this.TypeName, this.Id, BabylonConceptMetadata.Catalan, Catalan);
        }

        public override void Deserialize(IQueryResultSerializer deserializer, Dictionary<int, string> data)
        {
            base.Deserialize(deserializer, data);

            if (data.ContainsKey(BabylonConceptMetadata.Code))
                Code = data[BabylonConceptMetadata.Code];

            if (data.ContainsKey(BabylonConceptMetadata.Spanish))
                Spanish = data[BabylonConceptMetadata.Spanish];

            if (data.ContainsKey(BabylonConceptMetadata.English))
                English = data[BabylonConceptMetadata.English];

            if (data.ContainsKey(BabylonConceptMetadata.Catalan))
                Catalan = data[BabylonConceptMetadata.Catalan];
        }

        #endregion

        public string GetTranslatedConcept(int languageType)
        {
            var output = string.Empty;
            switch (languageType)
            {
                case LanguageTypes.English:
                    output = English;
                    break;
                case LanguageTypes.Catalan:
                    output = Catalan;
                    break;
                case LanguageTypes.Spanish:
                default:
                    output = Spanish;
                    break;
            }

            return output == null ? string.Empty : output;
        }
    }

    public static class BabylonConceptMetadata
    {
        public const int Code = EntityMetadata.Last + 1;
        public const int Spanish = Code + 1;
        public const int English = Spanish + 1;
        public const int Catalan = English + 1;
        public const int Last = Catalan;
    }

    public static class LanguageTypes
    {
        public const int Spanish = 1;
        public const int English = Spanish + 1;
        public const int Catalan = English + 1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Lib.Babylon
{
    public class BabylonConceptDto
    {
        public string Spanish { get; set; }
        public string English { get; set; }
        public string Catalan { get; set; }

        public string GetTranslation(int language)
        {
            switch (language)
            {
                case LanguageTypes.English:
                    return English;

                case LanguageTypes.Catalan:
                    return Catalan;

                default:
                case LanguageTypes.Spanish:
                    return Spanish;
            }
        }
    }
}

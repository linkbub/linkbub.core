﻿using Common.Lib.Core;
using Common.Lib.Infrastructure;
using Common.Lib.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Common.Lib.UI
{
    public class ViewModelBase
    {
        public IDependencyContainer DepCon { get; set; }

        public Dictionary<string, Tuple<Func<object>, Action<object>>> PropertiesHandler { get; set; }

        public Dictionary<string, Action> MethodsCallers { get; set; }

        public string Id { get; set; }

        public ViewModelBase()
        {
            PropertiesHandler = new Dictionary<string, Tuple<Func<object>, Action<object>>>();
            MethodsCallers = new Dictionary<string, Action>();

            BindProperties();
            BindMethods();
        }

        public virtual void Instantiate()
        {

        }

        public void NotifyPropertyChanged(object value, [CallerMemberName] string propertyName = null)
        {
            if (UiHelper.UpdateProperty != null)
            {
                if (value is IList || value is Entity)
                {
                    UiHelper.UpdateProperty(Id, propertyName, CommonSerializer.SerializeAction(value));
                }
                else
                {
                    UiHelper.UpdateProperty(Id, propertyName, value.ToString());
                }
                UiHelper.UpdateEnvironment();
            }
        }

        private void BindProperties()
        {
            var t = this.GetType();
            foreach (var prop in t.GetProperties())
            {
                object[] attributes = prop.GetCustomAttributes(typeof(BindPropertyAttribute), true);
                if (attributes.Length == 1)
                {
                    PropertiesHandler.Add(prop.Name, new Tuple<Func<object>,
                        Action<object>>(
                        () =>
                        {
                            var output = ProcessReadProperty(prop);
                            return output;
                        },

                        (value) =>
                        {
                            if (prop.GetValue(this) != value)
                                ProcessWriteProperty(value, prop);
                        }));
                }
            }
        }

        private void BindMethods()
        {
            var t = this.GetType();
            foreach (var method in t.GetMethods())
            {
                object[] attributes = method.GetCustomAttributes(typeof(BindMethodAttribute), true);
                if (attributes.Length == 1)
                {
                    MethodsCallers.Add(method.Name, () => { method.Invoke(this, null); });
                }
            }
        }

        private object ProcessReadProperty(PropertyInfo prop)
        {
            var value = prop.GetValue(this);

            if (prop.PropertyType == typeof(int))
            {
                if (value == null)
                    return 0;

                if (value is int || value is double || value is float || value is bool)
                    return (int)value;

                else
                {
                    int.TryParse(value.ToString(), out int output);
                    return output;
                }
            }

            if (prop.PropertyType == typeof(double))
            {
                if (value == null)
                    return 0.0;

                if (value is int || value is double || value is float || value is bool)
                    return (double)value;

                else
                {
                    double.TryParse(value.ToString(), out double output);
                    return output;
                }
            }

            if (prop.PropertyType == typeof(float))
            {
                if (value == null)
                    return 0.0;

                if (value is int || value is double || value is float || value is bool)
                    return (double)value;

                else
                {
                    float.TryParse(value.ToString(), out float output);
                    return output;
                }
            }

            if (prop.PropertyType == typeof(bool))
            {
                if (value == null)
                    return false;

                if (value is bool)
                    return (bool)value;

                if (value is int || value is double || value is float)
                    return (int)value > 0;
            }

            if (prop.PropertyType == typeof(List<string>))
            {
                if (value == null || string.IsNullOrEmpty(value.ToString()))
                    return new List<string>();
                else
                    return CommonSerializer.DeserializeAction<List<string>>(value.ToString());
            }

            if (value == null)
                return string.Empty;
            else
                return value.ToString();
        }

        private void ProcessWriteProperty(object value, PropertyInfo prop)
        {
            if (prop.PropertyType == typeof(int))
            {
                if (value == null)
                {
                    value = 0;
                }
                else
                {
                    if (value is int || value is bool || value is double || value is float)
                    {
                        value = (int)value;
                    }
                    else
                    {
                        int.TryParse(value.ToString(), out int output);
                        value = output;
                    }
                }
            }

            else if (prop.PropertyType == typeof(double))
            {
                if (value == null)
                {
                    value = 0.0;
                }
                else
                {
                    if (value is int || value is bool || value is double || value is float)
                    {
                        value = (double)value;
                    }
                    else
                    {
                        double.TryParse(value.ToString(), out double output);
                        value = output;
                    }
                }
            }
            else if (prop.PropertyType == typeof(float))
            {
                if (value == null)
                {
                    value = 0.0f;
                }
                else
                {
                    if (value is int || value is bool || value is double || value is float)
                    {
                        value = (float)value;
                    }
                    else
                    {
                        float.TryParse(value.ToString(), out float output);
                        value = output;
                    }
                }
            }
            else if (prop.PropertyType == typeof(bool))
            {
                if (value == null)
                {
                    value = false;
                }
                else
                {
                    if (value is int || value is bool || value is double || value is float)
                    {
                        value = (bool)value;
                    }
                    else
                    {
                        bool.TryParse(value.ToString(), out bool output);
                        value = output;
                    }
                }
            }
            else if (prop.PropertyType == typeof(string))
            {
                if (value == null)
                {
                    value = string.Empty;
                }
                else
                {
                    value = value.ToString();
                }
            }
            else
            {
                if (value != null)
                {
                    if (value is string)
                    {
                        // lo intentamos convertir a su objeto
                        value = CommonSerializer.DeserializeAsObjectAction(value.ToString(), prop.PropertyType);
                    }
                }
            }

            prop.SetValue(this, value);
        }
    }
}

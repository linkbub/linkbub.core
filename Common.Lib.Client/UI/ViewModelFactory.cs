﻿using Common.Lib.Core;
using System;
using System.Collections.Generic;

namespace Common.Lib.UI
{
    public class ViewModelFactory
    {
        public Dictionary<string, Func<ViewModelBase>> VMConstructors { get; set; }

        public Dictionary<string, ViewModelBase> CurrentViewModels { get; set; }

        public ViewModelFactory()
        {
            VMConstructors = new Dictionary<string, Func<ViewModelBase>>();
            CurrentViewModels = new Dictionary<string, ViewModelBase>();
        }

        public void RegisterViewModel<T>() where T : ViewModelBase, new()
        {
            var t = typeof(T).Name;
            if (VMConstructors.ContainsKey(t))
            {
                VMConstructors[t] = () => { return new T(); };
            }
            else
            {
                VMConstructors.Add(t, () => { return new T(); });
            }
        }

        public void ConstructViewModel(ViewModelInfo vmInfo)
        {
            if (VMConstructors.ContainsKey(vmInfo.Type))
            {
                var vm = VMConstructors[vmInfo.Type]();
                vm.Id = vmInfo.Id;
                vm.DepCon = Entity.Context.DepCon;

                CurrentViewModels.Add(vm.Id, vm);
                vm.Instantiate();

                UiHelper.UpdateEnvironment();
            }
            else
            {
                throw new Exception($"ViewModel type {vmInfo.Type} not found");
            }
        }

        public void SetVmProperty(ActionInfo info)
        {
            //UiHelper.Prompt($"ViewModelFactory SetVmProperty {info.Value}");
            var vm = CurrentViewModels[info.VmId];

            //UiHelper.Prompt($"ViewModelFactory SetVmProperty vm exists:{vm != null}");
            var setter = vm.PropertiesHandler[info.Name].Item2;

            //UiHelper.Prompt($"ViewModelFactory SetVmProperty setter exists:{setter != null}");

            setter(info.Value);
            //UiHelper.Prompt($"ViewModelFactory SetVmProperty property assigned with value { vm.PropertiesHandler[info.Name].Item1().ToString()}");
        }

        public string GetVmProperty(ActionInfo info)
        {
            if (CurrentViewModels.Count == 0)
            {
                // this happens when the VMFactory is not registered
                return string.Empty;
            }
            else
            {
                if (!CurrentViewModels.ContainsKey(info.VmId))
                {
                    //UiHelper.Prompt($"VMs count:{CurrentViewModels.Count}");
                    return string.Empty;
                }
                else
                {
                    var vm = CurrentViewModels[info.VmId];
                    var getter = vm.PropertiesHandler[info.Name].Item1;

                    var output = getter();
                    return output == null ? string.Empty : output.ToString();
                }

            }
        }

        public void CallVmMethod(ActionInfo info)
        {
            var vm = CurrentViewModels[info.VmId];
            var action = vm.MethodsCallers[info.Name];

            action();
        }
    }

    public class ViewModelInfo
    {
        public string Id { get; set; }

        public string Type { get; set; }

        public ViewModelInfo()
        {

        }
    }

    public class ActionInfo
    {
        public string VmId { get; set; }

        public ActionTypes ActionType { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public ActionInfo()
        {

        }

        public enum ActionTypes
        {
            Property = 1,
            Method = 2
        }
    }

    public static class UiHelper
    {
        public static Action<string> Prompt { get; set; }

        public static Action UpdateEnvironment { get; set; }

        public static Action<string, string, string> UpdateProperty { get; set; }
        public static Action<string> SetVMRegistered { get; set; }
    }

}

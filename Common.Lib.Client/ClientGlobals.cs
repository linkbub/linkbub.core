﻿using Common.Lib.Authentication;
using Common.Lib.Core.Context;

namespace Common.Lib.Client
{
    public class ClientGlobals
    {
        public ClientInfo ClientInfo { get; set; }

        public User LogonUser { get; set; }

        public string CurrentHost { get; set; }
    }
}

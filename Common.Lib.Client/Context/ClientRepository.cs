﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Serialization;
using Common.Lib.Services;
using Common.Lib.Services.Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Lib.Context
{
    public class ClientRepository<T> : IClientRepository<T> where T : Entity, new()
    {
        public bool PopulateBabylonConcepts { get; set; }
        public bool QueryMustGiveResult { get; set; }
        public bool ForceServerCall { get; set; }

        public int SelectedLanguage { get; set; } = 1;
        public IDependencyContainer DepCon { get; set; }
        
        public IUnitOfWork UoW { get; set; }

        public int RequestedPage { get; set; } = -1;
        public int PageSize { get; set; } = 0;
        public Guid QueryId { get; set; } = Guid.NewGuid();

        public ClientRepository()
        {

        }

        #region Sync operations are not allowed in client mode yet

        public Entity AddObject(Entity entity)
        {
            throw new NotImplementedException();
        }

        public Entity UpdateObject(Entity entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteObject(Entity entity)
        {
            throw new NotImplementedException();
        }

        public bool DeleteObject(Guid id)
        {
            throw new NotImplementedException();
        }

        public T Find(Guid id)
        {
            throw new NotImplementedException();
        }

        public T Add(T entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Guid id)
        {
            throw new NotImplementedException();
        }
        public T Update(T entity)
        {
            throw new NotImplementedException();
        }
        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public QueryResult Where(IExpressionBuilder condition, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        {
            throw new NotImplementedException();
        }

        public QueryResult FirstOrDefault(IExpressionBuilder condition, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        {
            throw new NotImplementedException();
        }

        public QueryResult Any(IExpressionBuilder condition)
        {
            throw new NotImplementedException();
        }


        public QueryResult Find(IExpressionBuilder condition, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        {
            throw new NotImplementedException();
        }

        public Entity FindObject(Guid id, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            throw new NotImplementedException();
        }

        public QueryResult Count(IExpressionBuilder condition)
        {
            throw new NotImplementedException();
        }

        public QueryResult GetAll(ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None, HashSet<Guid> addedIds = null)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IClientRepository

        public async Task<QueryResult<T>> FirstOrDefaultAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            return await FirstOrDefaultAsync(EmptyExpression.Create(), actionInfo, trace, childrenPolicyType);
        }

        public async Task<QueryResult<int>> CountAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await CountAsync(EmptyExpression.Create(), actionInfo, trace);
        }

        public async Task<QueryResult> AnyAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await AnyAsync(EmptyExpression.Create(), actionInfo, trace);
        }
        #endregion

        public async Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.HandleEntity,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                AddOrUpdateSingleEntity = new AddOrUpdateSingleEntity()
            };

            paramsCarrier.AddOrUpdateSingleEntity.SetChanges(entity);

            var svc = new ServiceInvoker();
            var svcr = await svc.Send<T>(paramsCarrier);

            return svcr;
        }

        public async Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.HandleEntity,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                AddOrUpdateSingleEntity = new AddOrUpdateSingleEntity()
            };

            paramsCarrier.AddOrUpdateSingleEntity.SetChanges(entity);

            var svc = new ServiceInvoker();
            var svcr = await svc.Send<T>(paramsCarrier);

            return svcr;
        }

        public async Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await DeleteAsync(entity.Id, actionInfo, trace);
        }        

        public async Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.HandleEntity,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                DeleteSingleEntity = new DeleteSingleEntity()
                {
                    EntityId = id,
                    TypeName = typeof(T).FullName
                }
            };

            var svc = new ServiceInvoker();
            var svcr = await svc.Send<T>(paramsCarrier);

            return svcr;
        }
        
        public async Task<List<T>> ToListAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            throw new NotImplementedException();
        }

        public async Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            if (trace != null)
                trace.AddStep($"Pprepare FindAsync for repo {typeof(T).Name}");

            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.QueryRepository,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                QueryRepository = new QueryRepository()
                {
                    PopulateBabylonConcepts = this.PopulateBabylonConcepts,
                    QueryType = RepositoryQueryTypes.Find,
                    RepoType = typeof(T).FullName,
                    ChildrenPolicyType = childrenPolicyType
                }
            };

            paramsCarrier.QueryRepository.ExpressionBuilder = Entity_Id_eq_value.Create(id);

            var svc = new ServiceInvoker();

            if (trace != null)
                trace.AddStep($"call send for FindAsync params");
            var svcr = await svc.Send<T>(paramsCarrier, DepCon.ResolveAndConstruct<IQueryResultSerializer>());
            return svcr;
        }

        public async Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            if (trace != null)
                trace.AddStep($"Pprepare FirstOrDefaultAsync for repo {typeof(T).Name}");

            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.QueryRepository,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                QueryRepository = new QueryRepository()
                {
                    PopulateBabylonConcepts = this.PopulateBabylonConcepts,
                    QueryType = RepositoryQueryTypes.FirstOrDefault,
                    RepoType = typeof(T).FullName,
                    ChildrenPolicyType = childrenPolicyType
                }
            };

            paramsCarrier.QueryRepository.ExpressionBuilder = condition;

            var svc = new ServiceInvoker();

            if (trace != null)
                trace.AddStep($"call send for FirstOrDefaultAsync params");

            var svcr = await svc.Send<T>(paramsCarrier, DepCon.ResolveAndConstruct<IQueryResultSerializer>());
            return svcr;
        }

        public async Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (trace != null)
                trace.AddStep($"Pprepare FirstOrDefaultAsync for repo {typeof(T).Name}");

            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.QueryRepository,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                QueryRepository = new QueryRepository()
                {
                    QueryType = RepositoryQueryTypes.Count,
                    RepoType = typeof(T).FullName
                }
            };

            paramsCarrier.QueryRepository.ExpressionBuilder = condition;

            var svc = new ServiceInvoker();

            if (trace != null)
                trace.AddStep($"call send for FirstOrDefaultAsync params");

            var svcr = await svc.Send<int>(paramsCarrier);
            return svcr;
        }

        public async Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (trace != null)
                trace.AddStep($"Pprepare AnyAsync for repo {typeof(T).Name}");

            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.QueryRepository,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                QueryRepository = new QueryRepository()
                {
                    QueryType = RepositoryQueryTypes.Any,
                    RepoType = typeof(T).FullName
                }
            };

            paramsCarrier.QueryRepository.ExpressionBuilder = condition;

            var svc = new ServiceInvoker();

            if (trace != null)
                trace.AddStep($"call send for FirstOrDefaultAsync params");

            var svcr = await svc.Send(paramsCarrier);
            return svcr;
        }

        public async Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            if (trace != null)
                trace.AddStep($"Pprepare WhereAsync for repo {typeof(T).Name}");

            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.QueryRepository,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                QueryRepository = new QueryRepository()
                {
                    PopulateBabylonConcepts = this.PopulateBabylonConcepts,
                    QueryType = RepositoryQueryTypes.GetAll,
                    RepoType = typeof(T).FullName,
                    QueryId = this.QueryId,
                    PageSize = PageSize,
                    PageNumber = RequestedPage,
                    ChildrenPolicyType = childrenPolicyType
                }
            };

            // Paging work the next way: 
            // if PageSize > 0 paging will be requested, 
            // if PageNumber == -1 next page will be requested,
            // if PageNumber >= 0 specific page number will be requested,
            
            var svc = new ServiceInvoker();

            if (trace != null)
                trace.AddStep($"call send for where params");

            var svcr = await svc.Send<List<T>>(paramsCarrier, DepCon.ResolveAndConstruct<IQueryResultSerializer>());
            
            if (svcr.Translations != null)
            {
                foreach (var entity in svcr.Value)
                {
                    var entityTranslations = svcr.Translations.Where(x => x.EntityId == entity.Id);
                    entity.AssignTranslations(entityTranslations);
                }
            }

            return svcr;            
        }

        public async Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
        {
            if (trace != null)
                trace.AddStep($"Pprepare WhereAsync for repo {typeof(T).Name}");

            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.QueryRepository,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                QueryRepository = new QueryRepository()
                {
                    PopulateBabylonConcepts = this.PopulateBabylonConcepts,
                    QueryType = RepositoryQueryTypes.Where,
                    RepoType = typeof(T).FullName,
                    QueryId = this.QueryId,
                    PageSize = PageSize,
                    PageNumber = RequestedPage,
                    ChildrenPolicyType = childrenPolicyType
                }
            };

            // Paging work the next way: 
            // if PageSize > 0 paging will be requested, 
            // if PageNumber == -1 next page will be requested,
            // if PageNumber >= 0 specific page number will be requested,

            paramsCarrier.QueryRepository.ExpressionBuilder = condition;

            var svc = new ServiceInvoker();

            if (trace != null)
                trace.AddStep($"call send for where params");

            var svcr = await svc.Send<List<T>>(paramsCarrier, DepCon.ResolveAndConstruct<IQueryResultSerializer>());

            if (svcr.Translations != null)
            {
                foreach (var entity in svcr.Value)
                {
                    var entityTranslations = svcr.Translations.Where(x => x.EntityId.ToString() == entity.Id.ToString());
                    entity.AssignTranslations(entityTranslations);
                }
            }

            return svcr;
        }
            

        public IRepository<T> GetNextPage(int pageSize = 20)
        {
            this.PageSize = pageSize;
            return this;
        }

        public IRepository<T> GetPage(int pageNum, int pageSize = 20)
        {
            this.RequestedPage = pageNum;
            this.PageSize = pageSize;
            return this;
        }

        /// <summary>
        /// Returns the query paged and the page 0
        /// </summary>
        /// <param name="pageSize">Page Size</param>
        /// <returns>The repository to keep asking fluently</returns>
        public IRepository<T> GetPaged(int pageSize = 20)
        {
            return GetPage(0, pageSize);
        }       

        public void Dispose()
        {
            //Todo: request dispose paging to server in the case
        }
    }
}

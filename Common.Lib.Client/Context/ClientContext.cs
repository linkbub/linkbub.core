﻿using Common.Lib.Core.Context;
using Common.Lib.Infrastructure;

namespace Common.Lib.Context
{
    public class ClientContext : IContext
    {
        public ContextModesTypes ContextMode { get; set; }
        public IDependencyContainer DepCon { get; set; }

        public ClientContext(IDependencyContainer depCon)
        {
            ContextMode = ContextModesTypes.ClientMode;
            DepCon = depCon;
        }

        public IUnitOfWork GetUnitOfWork()
        {
            return DepCon.ResolveAndConstruct<IUnitOfWork>();
        }
    }
}

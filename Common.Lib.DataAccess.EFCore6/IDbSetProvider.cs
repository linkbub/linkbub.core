﻿using Common.Lib.DataAcces.EFCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Common.Lib.Core.Context
{
    public interface IDbSetProvider : IDisposable
    {
        DbSet<T> ResolveDbSet<T>() where T : Entity;
        object ResolveDbSet(string typeName);
        int SaveChanges();
        Task<int> SaveChangesAsync();

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using Microsoft.EntityFrameworkCore;

namespace Common.Lib.DataAcces.EFCore
{
    public abstract class CachedDbSetEFWrapper
    {
        public static MemDbSet<Entity> CacheItems { get; set; }

        public abstract void InitCacheItems();

        public abstract void UnloadFromMemoryPendingItems();
    }

    public class CachedDbSetEFWrapper<T> : CachedDbSetEFWrapper, IDbSet<T> where T : Entity
    {
        public IDependencyContainer DepCon
        {
            get
            {
                return _depCon;
            }
            set
            {
                _depCon = value;
                if (!BelongsToUnitOfWork)
                {
                    DbSetProvider = _depCon.ResolveAndConstruct<CommonEfDbContext>();
                    DbSet = DbSetProvider.ResolveDbSet<T>();
                }
            }
        }
        IDependencyContainer _depCon;

        IDbSetProvider DbSetProvider { get; set; }
        public DbSet<T> DbSet { get; set; }

        public bool BelongsToUnitOfWork { get; set; }

        Dictionary<Guid, T> PendingToConfirmAddToCache { get; set; }

        Dictionary<Guid, T> PendingToConfirmUpdateToCache { get; set; }

        List<T> PendingToConfirmRemoveFromCache { get; set; }

        public CachedDbSetEFWrapper()
        {
        }

        public CachedDbSetEFWrapper(DbSet<T> dbSet)
            : this()
        {
            BelongsToUnitOfWork = true;
            DbSet = dbSet;

            PendingToConfirmAddToCache = new Dictionary<Guid, T>();
            PendingToConfirmUpdateToCache = new Dictionary<Guid, T>();
            PendingToConfirmRemoveFromCache = new List<T>();
        }

        #region  Enumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return CacheItems.OfType<T>().GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return CacheItems.OfType<T>().GetEnumerator();
        }
        #endregion
        
        #region Add
        public T Add(T entity)
        {
            return AddAsync(entity).Result.Value;
        }

        public async Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var output = (await DbSet.AddAsync(entity)).Entity;
            if (output != null)
            {
                CacheItems.Add(output);
                if (!BelongsToUnitOfWork)
                {
                    await DbSetProvider.SaveChangesAsync();
                }
                else
                {
                    PendingToConfirmAddToCache.Add(output.Id, output);
                }
            }

            return new QueryResult<T>() { IsSuccess = true, Value = output};
        }


        public async Task<QueryResult<Entity>> AddObjectAsync(Entity entity)
        {
            return (await AddAsync(entity as T)).CastTo<Entity>();
        }

        #endregion

        #region  Update

        public T Update(T entity)
        {
            return UpdateAsync(entity).Result.Value;
        }


        public async Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (entity.ParentEntity != null)
                entity.ParentEntity = null;

            var efEntity = await DbSet.FindAsync(entity.Id);
            entity.ParentEntity = efEntity;

            var entityAsChanges = entity.GetChanges();
            efEntity.ApplyChanges(entityAsChanges.Changes);

            var output = DbSet.Update(efEntity).Entity;
            if (output != null)
            {
                CacheItems.Update(output);
                if (!BelongsToUnitOfWork)
                {
                    await DbSetProvider.SaveChangesAsync();
                }
                else
                {
                    PendingToConfirmUpdateToCache.Add(output.Id, output);
                }
            }

            return new QueryResult<T>() { IsSuccess = true, Value = output };
        }

        public async Task<QueryResult<Entity>> UpdateObjectAsync(Entity entity)
        {
            return (await UpdateAsync(entity as T)).CastTo<Entity>();
        }

        #endregion

        #region Delete

        public bool Delete(T entity)
        {
            return Delete(entity.Id);
        }
        public bool Delete(Guid id)
        {
            var entityToRemove = DbSet.Find(id);
        
            var result = DbSet.Remove(entityToRemove).Entity != null;

            if (result)
            {
                if (!BelongsToUnitOfWork)
                {
                    DbSetProvider.SaveChanges();
                }
                else
                {
                    PendingToConfirmRemoveFromCache.Add(entityToRemove);
                }
                CacheItems.Delete(entityToRemove);
            }

            return result;
        }

        public Task<bool> DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        



        public async Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            throw new NotImplementedException();
        }

        public async Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Find
        public T Find(Guid id)
        {
            return CacheItems.Find(id) as T;
        }

        public async Task<QueryResult<Entity>> FindObjectAsync(Guid mainEntityId)
        {
            return (await FindAsync(mainEntityId)).CastTo<Entity>();
        }
        #endregion
        
        #region FirstOrDefault
        

        public async Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var exp = condition.Build<T, bool>();
                var o = CacheItems.OfType<T>().FirstOrDefault(exp);

                return new QueryResult<T>() { IsSuccess = o != null, Value = o };
            });
        }
        #endregion

        public async Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {            
            var exp = condition.CreateExpression<T, bool>();

            return new QueryResult() { IsSuccess = await CacheItems.OfType<T>().AsQueryable().AnyAsync(exp) };
        }

        public async Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await Task.Run(() =>
            {
                var exp = condition.Build<T, bool>();
                var o = CacheItems.OfType<T>().Where(exp).ToList();

                return new QueryResult<List<T>>() { IsSuccess = o != null, Value = o };
            });            
        }

        public async Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await WhereAsync(EmptyExpression.Create());
        }

        public async Task<List<T>> ToListAsync()
        {
            return await Task.Run(() =>
            {
                return CacheItems.OfType<T>().ToList();
            });
        }

        public void Dispose()
        {
            DbSetProvider.Dispose();
        }

        public async Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var output = new QueryResult<T>()
            {
                IsSuccess = true,
                Value = (await CacheItems.FindAsync(id)).Value as T
            };

            return output;
        }

        public override void InitCacheItems()
        {
            if (CacheItems == null)
            {
                CacheItems = new MemDbSet<Entity>();
            }

            if (CacheItems.OfType<T>().Count() == 0)
            {
                foreach (var item in DbSet)
                {
                    CacheItems.Add(item);
                }
            }
        }

        public override void UnloadFromMemoryPendingItems()
        {
            foreach (var entity in PendingToConfirmAddToCache)
            {
                CacheItems.Delete(entity.Key);
                CacheItems.Add(entity.Value);
            }

            foreach(var entity in PendingToConfirmUpdateToCache)
            {
                CacheItems.Delete(entity.Key);
                CacheItems.Add(entity.Value);
            }

            foreach (var entity in PendingToConfirmRemoveFromCache)
            {
                CacheItems.Add(entity);
            }
        }

        public async Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            //var exp = condition.Build<T, bool>();
            var qr = await CacheItems.CountAsync(condition);
            return qr;
        }
    }
}

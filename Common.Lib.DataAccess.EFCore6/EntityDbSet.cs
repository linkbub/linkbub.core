﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Metadata;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Common.Lib.DataAcces.EFCore
{
    public class EntityDbSet<T> : DbSet<T>, IDbSet<T> where T : Entity
    {
        public IDependencyContainer DepCon
        {
            get
            {
                return _depCon;
            }
            set
            {
                _depCon = value;
                if (!BelongsToUnitOfWork)
                {
                    DbSetProvider = _depCon.ResolveAndConstruct<CommonEfDbContext>();
                    DbSet = DbSetProvider.ResolveDbSet<T>();
                }
            }
        }
        IDependencyContainer _depCon;

        IDbSetProvider DbSetProvider { get; set; }
        public DbSet<T> DbSet { get; set; }

        public bool BelongsToUnitOfWork { get; set; }


        public override IEntityType EntityType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public EntityDbSet()
        {
        }

        public EntityDbSet(DbSet<T> dbSet)
            : this()
        {
            BelongsToUnitOfWork = true;
            DbSet = dbSet;
        }

        #region  Enumerable


        #endregion

        #region Add
        public T Add(T entity)
        {
            return AddAsync(entity).Result.Value;
        }

        public async Task<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var output = (await DbSet.AddAsync(entity)).Entity;
            if (output != null)
            {
                if (!BelongsToUnitOfWork)
                {
                    await DbSetProvider.SaveChangesAsync();
                }
            }

            return new QueryResult<T>() { IsSuccess = true, Value = output };
        }


        public async Task<QueryResult<Entity>> AddObjectAsync(Entity entity)
        {
            return (await AddAsync(entity as T)).CastTo<Entity>();
        }

        #endregion

        #region  Update

        public T Update(T entity)
        {
            return UpdateAsync(entity).Result.Value;
        }


        public async Task<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (entity.ParentEntity != null)
                entity.ParentEntity = null;

            var efEntity = await DbSet.FindAsync(entity.Id);
            entity.ParentEntity = efEntity;

            var entityAsChanges = entity.GetChanges();
            efEntity.ApplyChanges(entityAsChanges.Changes);

            var output = DbSet.Update(efEntity).Entity;
            if (output != null)
            {
                if (!BelongsToUnitOfWork)
                {
                    await DbSetProvider.SaveChangesAsync();
                }
            }

            return new QueryResult<T>() { IsSuccess = true, Value = output };
        }

        public async Task<QueryResult<Entity>> UpdateObjectAsync(Entity entity)
        {
            return (await UpdateAsync(entity as T)).CastTo<Entity>();
        }

        #endregion

        #region Delete

        public bool Delete(T entity)
        {
            return Delete(entity.Id);
        }
        public bool Delete(Guid id)
        {
            var entityToRemove = DbSet.Find(id);

            var result = DbSet.Remove(entityToRemove).Entity != null;

            if (result)
            {
                if (!BelongsToUnitOfWork)
                {
                    DbSetProvider.SaveChanges();
                }
            }

            return result;
        }

        public Task<bool> DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            throw new NotImplementedException();
        }





        public async Task<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            throw new NotImplementedException();
        }

        public async Task<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Find
        public T Find(Guid id)
        {
            return DbSet.Find(id) as T;
        }

        public async Task<QueryResult<Entity>> FindObjectAsync(Guid mainEntityId)
        {
            return (await FindAsync(mainEntityId)).CastTo<Entity>();
        }
        #endregion

        #region FirstOrDefault


        public async Task<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var exp = condition.CreateExpression<T, bool>();
            var o = await DbSet.FirstOrDefaultAsync(exp);

            return new QueryResult<T>() { IsSuccess = o != null, Value = o };
        }
        #endregion

        public async Task<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var exp = condition.CreateExpression<T, bool>();

            return new QueryResult() { IsSuccess = await DbSet.AnyAsync(exp) };
        }

        public async Task<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var exp = condition.CreateExpression<T, bool>();
            var o = await DbSet.Where(exp).ToListAsync();

            return new QueryResult<List<T>>() { IsSuccess = o != null, Value = o };
        }

        public async Task<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return await WhereAsync(EmptyExpression.Create());
        }

        public async Task<List<T>> ToListAsync()
        {
            return await Task.Run(() =>
            {
                return DbSet.ToList();
            });
        }

        public void Dispose()
        {
            DbSetProvider.Dispose();
        }

        public async Task<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            return new QueryResult<T>()
            {
                IsSuccess = true,
                Value = await DbSet.FindAsync(id)
            };
        }

        public async Task<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var exp = condition.CreateExpression<T, bool>();
            return new QueryResult<int>() { IsSuccess = true, Value = await DbSet.CountAsync(exp) };
        }
    }
}

﻿using Common.Lib.Babylon;
using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Common.Lib.DataAcces.EFCore
{
    public class CommonEfDbContext : DbContext, IDbSetProvider, IDependencyInjectable
    {
        public DbSet<BabylonConcept> BabylonConcepts { get; set; }
        //public DbSet<User> User { get; set; }
        private CachedDbSetEFWrapper<BabylonConcept> BabylonConceptsWrapper
        {
            get
            {
                return _babylonConceptsWrapper ?? (_babylonConceptsWrapper = new CachedDbSetEFWrapper<BabylonConcept>(BabylonConcepts));
            }
        }
        CachedDbSetEFWrapper<BabylonConcept> _babylonConceptsWrapper;

        //private CachedDbSetEFWrapper<User> UsersWrapper
        //{
        //    get
        //    {
        //        return _usersWrapper ?? (_usersWrapper = new CachedDbSetEFWrapper<User>(User));
        //    }
        //}
        //CachedDbSetEFWrapper<User> _usersWrapper;


        public Dictionary<string, Func<object>> DbSets { get; set; }

        public Dictionary<Type, CachedDbSetEFWrapper> DbSetsWrappers { get; set; }
        public IDependencyContainer DepCon { get; set; }

        public CommonEfDbContext(DbContextOptions options)
               : base(options)
        {
            DbSets = new Dictionary<string, Func<object>>
            {
                { typeof(BabylonConcept).FullName, () => BabylonConcepts },
                //{ typeof(User).FullName, () => User },
            };

            DbSetsWrappers = new Dictionary<Type, CachedDbSetEFWrapper>
            {
                { typeof(BabylonConcept), new CachedDbSetEFWrapper<BabylonConcept>(BabylonConcepts) },
                //{ typeof(User), new CachedDbSetEFWrapper<User>(User) }
            };
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // add your own confguration here
            //EntityConfiguration.StaticConfigure(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new BabylonConceptConfiguration());
            //modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        public DbSet<T> ResolveDbSet<T>() where T : Entity
        {
            return ResolveDbSet(typeof(T).FullName) as DbSet<T>;
        }

        public object ResolveDbSet(string typeName)
        {
            var o = DbSets[typeName]();
            return o as object;
        }

        public Task<int> SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }

        public new int SaveChanges()
        {
            return base.SaveChanges();
        }

        public virtual void AddEntity(string type, Entity entity)
        {
            switch (type)
            {
                case "Common.Lib.Babylon.BabylonConcept":
                    BabylonConceptsWrapper.Add(entity as BabylonConcept);
                    break;
                //case "Common.Lib.Authentication.User":
                //    UsersWrapper.Add(entity as User);
                //    break;
            }
        }

        public virtual void UpdateEntity(string type, Entity entity)
        {
            switch (type)
            {
                case "Common.Lib.Babylon.BabylonConcept":
                    BabylonConceptsWrapper.Update(entity as BabylonConcept);
                    break;
                //case "Common.Lib.Authentication.User":
                //    UsersWrapper.Update(entity as User);
                //    break;
            }
        }

        public virtual Entity FindEntiy(string type, Guid id)
        {
            switch (type)
            {
                case "Common.Lib.Babylon.BabylonConcept":
                    return BabylonConceptsWrapper.Find(id);
                //case "Common.Lib.Authentication.User":
                //    return UsersWrapper.Find(id);
            }
            return null;
            
        }

        public void InitWrappersCacheItems()
        {
            try
            {
                foreach (var wrapper in DbSetsWrappers)
                {
                    wrapper.Value.InitCacheItems();
                }
            }
            catch (Exception e1)
            {
                var a = e1;
            }
        }

        public void UnloadToMemoryAfterTransactionFail()
        {
            foreach(var item in DbSetsWrappers)
            {
                item.Value.UnloadFromMemoryPendingItems();
            }
        }
    }
}

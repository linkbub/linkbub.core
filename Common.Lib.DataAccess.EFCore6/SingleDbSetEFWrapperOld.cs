﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Threading.Tasks;
//using Common.Lib.Core;
//using Common.Lib.Core.Context;
//using Common.Lib.Core.Metadata;
//using Common.Lib.Infrastructure;
//using Common.Lib.Infrastructure.ActionsResults;
//using Common.Lib.Infrastructure.Log;
//using Common.Lib.Services;
//using Microsoft.EntityFrameworkCore;

//namespace Common.Lib.DataAcces.EFCore
//{
//    public class SingleDbSetEFWrapper<T> : IDbSet<T> where T : Entity
//    {
//        public IDependencyContainer DepCon
//        {
//            get
//            {
//                return _depCon;
//            }
//            set
//            {
//                _depCon = value;
//                DbSetProvider = _depCon.ResolveAndConstruct<CommonEfDbContext>();
//                DbSet = DbSetProvider.ResolveDbSet<T>();
//            }
//        }
//        IDependencyContainer _depCon;

//        IDbSetProvider DbSetProvider { get; set; }
//        public DbSet<T> DbSet { get; set; }

//        public SingleDbSetEFWrapper()
//        {

//        }

//        #region  Enumerable

//        IEnumerator IEnumerable.GetEnumerator()
//        {
//            return DbSet.AsQueryable().GetEnumerator();
//        }

//        public IEnumerator<T> GetEnumerator()
//        {
//            return DbSet.AsQueryable().GetEnumerator();
//        }
//        #endregion
        
//        #region Add
//        public T Add(T entity)
//        {
//            var output = DbSet.Add(entity).Entity;
//            if (output != null)
//                DbSetProvider.SaveChanges();

//            return output;
//        }

//        public Promise<QueryResult<T>> AddAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
//        {
//            var output = new Promise<QueryResult<T>>();

//            var result = Add(entity);
//            output.Result = new QueryResult<T>() { Value = result, IsSuccess = result != null };

//            return output;
//        }

//        public Task<T> AddAsync(T entity)
//        {
//            var result = DbSet.Add(entity).Entity;

//            if(result != null)
//                return DbSetProvider.SaveChangesAsync().ContinueWith((r) =>
//                    {
//                        return result;
//                    });
//            else
//                return Task.Run(() => { return default(T); });
//        }

//        public void AddObject(Entity entity)
//        {
//            Add(entity as T);
//        }

//        #endregion

//        #region  Update

//        public T Update(T entity)
//        {
//            var output = DbSet.Update(entity).Entity;
//            if (output != null)
//                DbSetProvider.SaveChanges();

//            return output;
//        }

//        public Task<T> UpdateAsync(T entity)
//        {
//            var result = DbSet.Update(entity).Entity;

//            if (result != null)
//                return DbSetProvider.SaveChangesAsync()
//                    .ContinueWith((t) =>
//                    {
//                        return result;
//                    });
//            else
//                return Task.Run(() => { return default(T); });
//        }


//        public Promise<QueryResult<T>> UpdateAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
//        {
//            var output = new Promise<QueryResult<T>>();

//            var o = Update(entity);
//            output.Result = new QueryResult<T>() { IsSuccess = o != null, Value = o };

//            return output;
//        }

//        public void UpdateObject(Entity entity)
//        {
//            Update(entity as T);
//        }

//        #endregion

//        #region Delete

//        public bool Delete(T entity)
//        {
//            return Delete(entity.Id);
//        }
//        public bool Delete(Guid id)
//        {
//            var entityToRemove = DbSet.Find(id);
//            return DbSet.Remove(entityToRemove).Entity != null;
//        }

//        public Task<bool> DeleteAsync(T entity)
//        {
//            throw new NotImplementedException();
//        }

//        public Task<bool> DeleteAsync(Guid id)
//        {
//            throw new NotImplementedException();
//        }

        


//        public Promise<QueryResult> DeleteAsync(T entity, ActionInfo actionInfo = null, OperationInfo trace = null)
//        {
//            throw new NotImplementedException();
//        }

//        public Promise<QueryResult> DeleteAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null)
//        {
//            throw new NotImplementedException();
//        }

//        #endregion

//        #region Find
//        public T Find(Guid id)
//        {
//            return DbSet.Find(id);
//        }

//        public object FindObject(Guid mainEntityId)
//        {
//            return Find(mainEntityId);
//        }
//        #endregion
        
//        #region FirstOrDefault
//        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> condition)
//        {
//            return DbSet.FirstOrDefaultAsync(condition);
//        }

//        public Promise<QueryResult<T>> FirstOrDefaultAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
//        {
//            var output = new Promise<QueryResult<T>>();

//            var exp = condition.Build<T>();
//            var o = this.FirstOrDefault(exp);

//            output.Result = new QueryResult<T>() { IsSuccess = o != null, Value = o };

//            return output;
//        }
//        #endregion

//        public Promise<QueryResult> AnyAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
//        {
//            var output = new Promise<QueryResult>();

//            var exp = condition.Build<T>();

//            output.Result = new QueryResult() { IsSuccess = this.Any(exp) };

//            return output;
//        }

//        public Promise<QueryResult<List<T>>> WhereAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
//        {
//            var output = new Promise<QueryResult<List<T>>>();

//            var exp = condition.Build<T>();
//            var o = this.Where(exp).ToList();

//            output.Result = new QueryResult<List<T>>() { IsSuccess = o != null, Value = o };

//            return output;
//        }
//        public Promise<QueryResult<List<T>>> GetAllAsync(ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
//        {
//            var output = new Promise<QueryResult<List<T>>>();

//            var o = this.ToList();

//            output.Result = new QueryResult<List<T>>() { IsSuccess = o != null, Value = o };

//            return output;
//        }

//        public Task<List<T>> ToListAsync()
//        {
//            return DbSet.ToListAsync();
//        }

//        public Task<IQueryable<T>> ToQueryableAsync()
//        {
//            return Task.Run(() => { return DbSet.AsQueryable<T>(); });
//        }

//        public void Dispose()
//        {
//            DbSetProvider.Dispose();
//        }

//        public Promise<QueryResult<T>> FindAsync(Guid id, ActionInfo actionInfo = null, OperationInfo trace = null, ChildrenPolicyTypes childrenPolicyType = ChildrenPolicyTypes.None)
//        {
//            throw new NotImplementedException();
//        }

//        public Promise<QueryResult<int>> CountAsync(IExpressionBuilder condition, ActionInfo actionInfo = null, OperationInfo trace = null)
//        {
//            throw new NotImplementedException();
//        }
//    }
//}

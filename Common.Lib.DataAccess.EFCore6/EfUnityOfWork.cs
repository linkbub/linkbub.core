﻿using System;
using System.Collections.Generic;
using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Core.Tracking;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Infrastructure.Log;
using Common.Lib.Services;
using Microsoft.EntityFrameworkCore;

namespace Common.Lib.DataAcces.EFCore
{
    public class EFCoreUnitOfWork : IUnitOfWork
    {
        CommonEfDbContext EfDbContext { get; set; }

        IDbSetProvider DbSetProvider
        {
            get
            {
                return (IDbSetProvider)EfDbContext;
            }
        }

        public IDependencyContainer DepCon
        {
            get
            {
                return _depCon;
            }
            set
            {
                _depCon = value;
                EfDbContext = _depCon.ResolveAndConstruct<CommonEfDbContext>();
            }
        }
        IDependencyContainer _depCon;

        public List<UoWActInfo> UowActions { get; set; }

        public EFCoreUnitOfWork()
        {
            UowActions = new List<UoWActInfo>();
        }

        public bool AddEntitySave(Entity entity, ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            UowActions.Add(new UoWActInfo()
            {
                EntityChange = entity.GetChanges(),
                UserId = actionInfo == null ? string.Empty : actionInfo.UserId,
                DeviceId = actionInfo == null ? string.Empty : actionInfo.DeviceId,
                ActionTime = actionInfo == null ? DateTime.Now : actionInfo.ActionDate,
                ActionInfoType = ActionInfoTypes.Save
            });
            return true;
        }

        void CommitActions(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var changes = new List<EntityChanges>();
            var output = new QueryResult();

            var entities = new List<object>();

            foreach (var action in UowActions)
            {
                var type = action.EntityChange.EntityModelType;

                switch (action.ActionInfoType)
                {
                    case ActionInfoTypes.Save:

                        if (action.EntityChange.IsNew || action.EntityChange.MainEntityId == default(Guid) || EfDbContext.FindEntiy(type, action.EntityChange.MainEntityId) == null)
                        {
                            var entity = MetadataHandler.ModelsByTypeName[type].Constructor();
                            entity.ApplyChanges(action.EntityChange.Changes);
                            if (entity.Id == default(Guid))
                                entity.Id = action.EntityChange.MainEntityId != default(Guid) ? action.EntityChange.MainEntityId : Guid.NewGuid();

                            //entity.ValidationAction().Then = (qr) =>
                            //{
                            //    if (qr.IsSuccess)
                            //    {
                            EfDbContext.AddEntity(type, entity);
                            entities.Add(entity);
                            //    }
                            //    else
                            //    {
                            //        output = new QueryResult() { IsSuccess = false, Message = "error in entity save validation" };
                            //    }
                            //};
                        }
                        else
                        {
                            var entity = (EfDbContext.FindEntiy(type, action.EntityChange.MainEntityId) as Entity).CloneAction();
                            entity.ApplyChanges(action.EntityChange.Changes);

                            //entity.ValidationAction().Then = (qr) =>
                            //{
                            //    if (qr.IsSuccess)
                            //    {
                            EfDbContext.UpdateEntity(type, entity);
                            entities.Add(entity);
                            //    }
                            //    else
                            //    {
                            //        output = new QueryResult() { IsSuccess = false, Message = "error in entity save validation" };
                            //    }
                            //};
                        }

                        break;
                    default:
                    case ActionInfoTypes.Delete:
                        //Todo
                        throw new NotImplementedException();
                }
            }
        }

        public QueryResult Commit(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (UowActions.Count == 0)
                return new QueryResult() { IsSuccess = true };

            if (Entity.Context.ContextMode == ContextModesTypes.ServerMode)
            {
                CommitActions(actionInfo, trace);

                // Commit transaction if all commands succeed, transaction will auto-rollback
                // when disposed if either commands fails
                EfDbContext.SaveChanges();

                // Todo: implement error
                //EfDbContext.UnloadToMemoryAfterTransactionFail();

                // if no errors
                return new QueryResult() { IsSuccess = true };
            }
            else
            {
                throw new InvalidOperationException("commit sync is not allowed in client mode");
            }
        }

        public async Task<QueryResult> CommitAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            if (UowActions.Count == 0)
                return new QueryResult() { IsSuccess = true };

            if (Entity.Context.ContextMode == ContextModesTypes.ServerMode)
            {
                CommitActions(actionInfo, trace);

                // Commit transaction if all commands succeed, transaction will auto-rollback
                // when disposed if either commands fails
                await EfDbContext.SaveChangesAsync();

                // Todo: implement error
                //EfDbContext.UnloadToMemoryAfterTransactionFail();

                // if no errors
                return new QueryResult() { IsSuccess = true };
            }
            else
            {
                return await SendToServerAsync(actionInfo, trace);
            }
        }

        async Task<QueryResult> SendToServerAsync(ActionInfo actionInfo = null, OperationInfo trace = null)
        {
            var paramsCarrier = new ParamsCarrier
            {
                OperationType = OperationTypes.HandleEntity,
                RequestorId = actionInfo != null ? actionInfo.UserId : default(Guid).ToString(),
                Trace = trace,
                UnitOfWorkInfo = new Services.Operations.UnitOfWorkInfo()
            };

            paramsCarrier.UnitOfWorkInfo.SetChanges(this);

            var svc = new ServiceInvoker();
            var svcr = await svc.Send(paramsCarrier);

            return svcr;
        }

        public void Dispose()
        {
            EfDbContext.Dispose();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Common.Lib.Authentication;
using Common.Lib.Babylon;

namespace Common.Lib.DataAcces.EFCore
{
    public class BabylonConceptConfiguration : BabylonConceptConfiguration<BabylonConcept>
    {
        public override void Configure(EntityTypeBuilder<BabylonConcept> builder)
        {
            base.Configure(builder);
        }
    }

    public class BabylonConceptConfiguration<T> : EntityConfiguration<T> where T : BabylonConcept
    {
        public override void Configure(EntityTypeBuilder<T> builder)
        {
            base.Configure<T>(builder);
            builder.ToTable("BabylonConcepts");
        }
    }
}

﻿using Common.Lib.Core;
using Common.Lib.Core.Context;
using Common.Lib.Infrastructure;
using Common.Lib.Infrastructure.ActionsResults;
using Common.Lib.Serialization;
using Common.Lib.Services;
using Common.Lib.Services.Operations;
using Common.Lib.Services.Pagination;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Lib.Server
{
    public class ServerContext : IContext
    {
        public ContextModesTypes ContextMode { get; set; }
        public IDependencyContainer DepCon { get; set; }

        public ServerContext(IDependencyContainer dc)
        {
            ContextMode = ContextModesTypes.ServerMode;
            DepCon = dc;
        }

        public IUnitOfWork GetUnitOfWork()
        {
            return DepCon.ResolveAndConstruct<IUnitOfWork>();
        }

        public async Task<QueryResult> Execute(ParamsCarrier paramsCarrier)
        {
            switch (paramsCarrier.OperationType)
            {
                case OperationTypes.RegisterClient:
                    break;

                case OperationTypes.QueryRepository:
                    return await QueryRepository(paramsCarrier);

                case OperationTypes.HandleEntity:
                    return await HandleEntity(paramsCarrier);

                case OperationTypes.ServiceRequest:
                    return await ServiceRequest(paramsCarrier);

                default:
                    break;
            }

            return null;
        }

        async Task<QueryResult> QueryRepository(ParamsCarrier paramsCarrier)
        {
            var output = new QueryResult();
            QueryResult result = null;
            var condition = paramsCarrier.QueryRepository.ExpressionBuilder;

            using (var repo = DepCon.ResolveRepository(paramsCarrier.QueryRepository.RepoType) as IPersistanceRepository)
            {
                repo.PopulateBabylonConcepts = paramsCarrier.QueryRepository.PopulateBabylonConcepts;

                switch (paramsCarrier.QueryRepository.QueryType)
                {
                    case RepositoryQueryTypes.GetAll:
                        result = await repo.GetAllEntityAsync(null, null, paramsCarrier.QueryRepository.ChildrenPolicyType);
                        output.IsSuccess = true;
                        output.ObjectValue = result.Serialize(DepCon);
                        output.Translations = result.Translations;

                        break;

                    case RepositoryQueryTypes.Where:
                        result = paramsCarrier.QueryRepository.PageSize == 0 ?
                            await repo.WhereEntityAsync(condition, null, null, paramsCarrier.QueryRepository.ChildrenPolicyType) :
                            await PageQueryRepositoryResult(paramsCarrier.QueryRepository, repo);
                        output.IsSuccess = true;
                        output.ObjectValue = result.Serialize(DepCon);
                        output.Translations = result.Translations;

                        break;

                    case RepositoryQueryTypes.FirstOrDefault:
                        result = await repo.FirstOrDefaultEntityAsync(condition, null, null, paramsCarrier.QueryRepository.ChildrenPolicyType);
                        output.IsSuccess = true;
                        output.ObjectValue = result.Serialize(DepCon);
                        output.Translations = result.Translations;

                        break;

                    case RepositoryQueryTypes.Count:
                        result = await repo.CountEntityAsync(condition); 
                        output.IsSuccess = true;
                        output.ObjectValue = CommonSerializer.SerializeAction(result.ObjectValue);
                        break;

                    case RepositoryQueryTypes.Any:
                        result = await repo.AnyEntityAsync(condition);
                        output.IsSuccess = true;
                        output.ObjectValue = CommonSerializer.SerializeAction(result.ObjectValue);

                        break;

                    case RepositoryQueryTypes.Find:
                        result = await repo.FindEntityAsync((condition as Entity_Id_eq_value).Id, null, null, paramsCarrier.QueryRepository.ChildrenPolicyType);
                        output.IsSuccess = true;
                        output.ObjectValue = result.Serialize(DepCon);
                        output.Translations = result.Translations;

                        break;

                    default:
                        break;
                }
            }

            return output;
        }

        async Task<QueryResult> PageQueryRepositoryResult(QueryRepository queryRepository, IPersistanceRepository repository)
        {
            var pageHandler = DepCon.ResolveSingleton(() => new ResultsPaginationHandler());
            return await pageHandler.GetNextResults(queryRepository, repository);
        }

        async Task<QueryResult> ServiceRequest(ParamsCarrier paramsCarrier)
        {
            var serviceRequest = (ServiceRequest)CommonSerializer.
                                        DeserializeAsObjectAction(
                                                paramsCarrier.ServiceRequestInputParams, 
                                                ServiceHandler.ServicesRequestTypes[paramsCarrier.ServiceRequestType]);

            serviceRequest.DepCon = this.DepCon;

            return await serviceRequest.Execute(paramsCarrier.Trace);            
        }

        async Task<QueryResult> HandleEntity(ParamsCarrier paramsCarrier)
        {
            if (paramsCarrier.AddOrUpdateSingleEntity != null)
            {
                if (paramsCarrier.AddOrUpdateSingleEntity.EntityId == default(Guid))
                {
                    return await CreateEntity(paramsCarrier);
                }
                else
                {
                    return await UpdateEntity(paramsCarrier);
                }
            }
            else if (paramsCarrier.DeleteSingleEntity != null)
            {
                return await DeleteEntity(paramsCarrier);
            }
            else if (paramsCarrier.UnitOfWorkInfo != null)
            {
                return await ResolveUnitOfWork(paramsCarrier);
            }

            return null;
        }

        async Task<QueryResult> CreateEntity(ParamsCarrier paramsCarrier)
        {
            var changesInfos = paramsCarrier.AddOrUpdateSingleEntity.GetChanges();
            var changeInfo = changesInfos.FirstOrDefault();

            var entityToSave = MetadataHandler.ModelsByTypeName[changeInfo.TypeName].Constructor();
            entityToSave.ApplyChanges(changeInfo.Changes);

            var qrAdd = await entityToSave.SaveAction(null, paramsCarrier.Trace);
            return qrAdd;
        }

        async Task<QueryResult> UpdateEntity(ParamsCarrier paramsCarrier)
        {
            var changesInfos = paramsCarrier.AddOrUpdateSingleEntity.GetChanges();
            var changeInfo = changesInfos.FirstOrDefault();

            using (var repo = DepCon.ResolveRepository(changeInfo.TypeName) as IPersistanceRepository)
            {
                var entityToSave = (await repo.FindEntityAsync(paramsCarrier.AddOrUpdateSingleEntity.EntityId)).Value.CloneAction();
                entityToSave.ApplyChanges(changeInfo.Changes);

                var qrUpdate = await entityToSave.SaveAction(null, paramsCarrier.Trace);
                
                return qrUpdate;                
            }
        }

        async Task<QueryResult> DeleteEntity(ParamsCarrier paramsCarrier)
        {
            var deleteInfo = paramsCarrier.DeleteSingleEntity;

            using (var repo = DepCon.ResolveRepository(deleteInfo.TypeName) as IPersistanceRepository)
            {
                var entityToDelete = (await repo.FindEntityAsync(paramsCarrier.DeleteSingleEntity.EntityId)).Value;
                var qrDelete = await entityToDelete.DeleteAction(null, paramsCarrier.Trace);

                return qrDelete;
            };
        }

        async Task<QueryResult> ResolveUnitOfWork(ParamsCarrier paramsCarrier)
        {
            using (var uow = DepCon.ResolveAndConstruct<IUnitOfWork>())
            {
                uow.UowActions = paramsCarrier.UnitOfWorkInfo.UoWActInfos;
                return await uow.CommitAsync();
            }
        }
    }
}
